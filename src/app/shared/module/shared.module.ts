import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CalendarModule } from 'primeng/calendar';
import { TableModule } from 'primeng/table';
import { HeaderComponent } from '../../header/header.component';

import { SidebarModule } from 'primeng/sidebar';
import { SidebarComponent } from '../../header/components/sidebar/sidebar.component';
import { SafePipe } from '../pipe/safe.pipe';

@NgModule({
  declarations: [HeaderComponent, SidebarComponent, SafePipe],
  imports: [CommonModule, TableModule, CalendarModule, SidebarModule],
  exports: [
    HeaderComponent,
    SidebarComponent,
    TableModule,
    CalendarModule,
    SidebarModule
  ]
})
export class SharedModule {}
