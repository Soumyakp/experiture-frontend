import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  constructor(
    private messageService: MessageService,
    private commonService: CommonService
  ) {}

  showSuccess(message: string): void {
    this.messageService.add({
      severity: 'success',
      summary: 'Success Message',
      detail: message
    });
  }
  showInformation(message: string, summary = 'Information Message'): void {
    this.messageService.add({
      severity: 'info',
      summary,
      detail: message
    });
  }
  showError(message = 'Internal server error!'): void {
    this.messageService.add({
      severity: 'error',
      summary: 'Error Message',
      detail: message
    });
  }
  showExportSuccess(message: string): void {
    this.messageService.add({
      severity: 'success',
      summary: 'Success Message',
      detail: message,
      key: 'exp-queued'
    });
  }
}
