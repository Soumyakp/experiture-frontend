import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import * as FileSaver from 'file-saver';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';

const API_URL = `${environment.API_URL}/api/List/`;

export interface ParamInfo {
  userName: string;
  password: string;
  redirect?: string;
  rootUserId: number;
}

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  jsonFormatter = {
    stringify(cipherParams): any {
      // create json object with ciphertext
      const jsonObj = {
        ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64),
        iv: null,
        s: null
      };
      // optionally add iv or salt
      if (cipherParams.iv) {
        jsonObj.iv = cipherParams.iv.toString();
      }
      if (cipherParams.salt) {
        jsonObj.s = cipherParams.salt.toString();
      }
      // stringify json object
      return JSON.stringify(jsonObj);
    },
    parse(jsonStr): any {
      // parse json string
      const jsonObj = JSON.parse(jsonStr);
      // extract ciphertext from json object, and create cipher params object
      const cipherParams = CryptoJS.lib.CipherParams.create({
        ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct)
      });
      // optionally extract iv or salt
      if (jsonObj.iv) {
        cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv);
      }
      if (jsonObj.s) {
        cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s);
      }
      return cipherParams;
    }
  };
  constructor(private http: HttpClient) {}

  $progress = new Subject<boolean>();

  getExportFileName(listName: string): string {
    return `${listName}_Export_${new Date().getDate()}${
      new Date().getMonth() + 1
    }${new Date().getFullYear()}${new Date().getTime()}`;
  }

  downloadFile(buffer: ArrayBuffer, listName: string): void {
    const blob = new Blob([buffer], { type: 'application/ms-excel' });
    FileSaver.saveAs(blob, this.getExportFileName(listName) + '.csv');
  }

  getExportNotificationEmail(): Observable<any> {
    return this.http.post(
      `${API_URL}GetExportNotificationEmailbyUserID`,
      { userId: +sessionStorage.getItem('USER_ID') },
      this.httpOptions
    );
  }

  saveExportNotificationEmail(reqBody: {
    emailAddress: string;
    isRemember: boolean;
  }): Observable<any> {
    return this.http.post(
      `${API_URL}SaveExportNotificationEmail`,
      {
        userId: +sessionStorage.getItem('USER_ID'),
        ...reqBody
      },
      this.httpOptions
    );
  }

  encrypt(text: any): string {
    const cfg = {
      iv: CryptoJS.enc.Hex.parse(environment.secretKey.hexStr),
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC
    };
    const salt = CryptoJS.enc.Utf8.parse(environment.secretKey.salt);
    const pass = CryptoJS.enc.Utf8.parse(environment.secretKey.pass);
    const key128Bits1000Iterations = CryptoJS.PBKDF2(
      pass.toString(CryptoJS.enc.Utf8),
      salt,
      { keySize: 128 / 32, iterations: 1000 }
    );

    const encrypted = CryptoJS.AES.encrypt(text, key128Bits1000Iterations, cfg);
    // console.log(encrypted.toString());
    return encrypted.toString();
  }
  decrypt(text: any): any {
    // Creating the Vector Key
    const iv = CryptoJS.enc.Hex.parse(environment.secretKey.hexStr);
    // Encoding the Password in from UTF8 to byte array
    const pass = CryptoJS.enc.Utf8.parse(environment.secretKey.pass);
    // Encoding the Salt in from UTF8 to byte array
    const salt = CryptoJS.enc.Utf8.parse(environment.secretKey.salt);
    // Creating the key in PBKDF2 format to be used during the decryption
    const key128Bits1000Iterations = CryptoJS.PBKDF2(
      pass.toString(CryptoJS.enc.Utf8),
      salt,
      { keySize: 128 / 32, iterations: 1000 }
    );
    // Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
    const cipherParams = CryptoJS.lib.CipherParams.create({
      ciphertext: CryptoJS.enc.Base64.parse(text)
    });

    // Decrypting the string contained in cipherParams using the PBKDF2 key
    const decrypted = CryptoJS.AES.decrypt(
      cipherParams,
      key128Bits1000Iterations,
      { mode: CryptoJS.mode.CBC, iv, padding: CryptoJS.pad.Pkcs7 }
    );
    const decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
    return JSON.parse(decryptedData);
  }

  getParamInfo(): ParamInfo {
    const encryptInfo = sessionStorage.getItem('INFO');
    if (encryptInfo) {
      const decryptObj = this.decrypt(encryptInfo);
      return {
        userName: decryptObj.userName,
        password: decryptObj.password,
        rootUserId: decryptObj.rootUserId
      };
    } else {
      return undefined;
    }
  }
}
