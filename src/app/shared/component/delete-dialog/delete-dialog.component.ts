import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  @Output() deleteItem = new EventEmitter<boolean>();
  @Input() confirmMsg: string;
  @Input() confirmLabel: string;
  @Input() denyLabel: string;

  constructor() {}

  ngOnInit(): void {
    // 'YES, DELETE THIS VIEW'
    // 'NO, KEEP THE VIEW'
  }

  hideDialog(event): void {
    this.visibleChange.emit(false);
  }

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  onConfirm(): void {
    this.deleteItem.emit(true);
  }
}
