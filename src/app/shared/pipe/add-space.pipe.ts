import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addSpace'
})
export class AddSpacePipe implements PipeTransform {
  transform(value: string, exponent?: number): string {
    if (value?.length > exponent) {
      if (/\s/g.test(value)) {
        return value;
      } else {
        let newStr = '';
        const len = Math.floor(value.length / exponent);
        for (let i = 0; i <= len; i++) {
          newStr += `${value.slice(i * 22, (i + 1) * 22)} `;
        }
        return newStr;
      }
    } else {
      return undefined;
    }
  }
}
