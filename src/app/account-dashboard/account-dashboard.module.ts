import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AccountDashboardRoutingModule } from './account-dashboard-routing.module';
import { AccountDashboardComponent } from './account-dashboard/account-dashboard.component';
import { MyProgramComponent } from './account-dashboard/my-program/my-program.component';
import { TargetComponent } from './account-dashboard/target/target.component';
import { MyAccountComponent } from './account-dashboard/my-account/my-account.component';
import { SharedModule } from '../shared/module/shared.module';
import { LeadDashboardComponent } from './lead-dashboard/lead-dashboard.component';

@NgModule({
  declarations: [
    AccountDashboardComponent,
    MyProgramComponent,
    TargetComponent,
    LeadDashboardComponent,
    MyAccountComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    CommonModule,
    AccountDashboardRoutingModule,
    SharedModule
  ]
})
export class AccountDashboardModule {}
