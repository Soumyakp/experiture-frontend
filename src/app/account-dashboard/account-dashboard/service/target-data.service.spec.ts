import { TestBed } from '@angular/core/testing';

import { TargetDataService } from './target-data.service';

describe('TargetDataService', () => {
  let service: TargetDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TargetDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
