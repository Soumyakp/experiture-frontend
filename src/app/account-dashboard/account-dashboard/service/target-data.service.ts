import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Targetlist {LatestActivity;  Date;  ProfilePhoto; Name; Company; Title;}

@Injectable({
  providedIn: 'root'
})

export class TargetDataService {

  constructor(private http: HttpClient) { }

  getTagetList(){
    return this.http.get<any>('../../../../assets/target.json')
    .toPromise()
    .then(res =><Targetlist[]>res.data )
    .then(data => {return data;});
  }

}
