import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface Programlist {  progName;  startdate;  report;}
  
  
  
@Injectable({
  providedIn: 'root'
})
export class ProgramDataService {

  constructor(private http: HttpClient) { }

  getDasbhaordProgram()
  {
     return this.http.get<any>('../../../../assets/program.json')
                .toPromise()
                .then(res =><Programlist[]>res.data )
                .then(data => {return data;});
  }
}
