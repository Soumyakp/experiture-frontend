import { Component, OnInit } from '@angular/core';
import { TargetDataService,Targetlist } from '../service/target-data.service';

@Component({
  selector: 'app-dashboard-target',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.scss']
})

export class TargetComponent implements OnInit {
  targetlists : Targetlist[]
  constructor(private targetdataservice : TargetDataService) { }

  ngOnInit(){
    this.targetdataservice.getTagetList()
    .then(targetlists => this.targetlists = targetlists)
  }

}
