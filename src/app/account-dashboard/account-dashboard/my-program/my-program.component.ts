import { Component, OnInit } from '@angular/core';
import {ProgramDataService,Programlist} from '../service/program-data.service'

@Component({
  selector: 'app-my-program',
  templateUrl: './my-program.component.html',
  styleUrls: ['./my-program.component.scss']
})
export class MyProgramComponent implements OnInit {
  public show:boolean = false;

  programlists : Programlist[]
  
  activityValues: number[] = [0, 100];

  constructor(private porgramdataservice : ProgramDataService) { }

  ngOnInit(){
    this.porgramdataservice
        .getDasbhaordProgram()
        .then(programlists => this.programlists = programlists)
  }
  toggle() {
    this.show = !this.show;
  }

}
