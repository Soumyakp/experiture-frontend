import { Component,  OnInit} from '@angular/core';
import {
  ElementRef,
  HostListener,
  ViewChild
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-lead-dashboard',
  templateUrl: './lead-dashboard.component.html',
  styleUrls: ['./lead-dashboard.component.scss']
})
export class LeadDashboardComponent implements OnInit {
  isSelected: boolean = false;
  fromdateValue: any;
  todateValue: any;

  showMenu: boolean;
  @ViewChild('menuRef') menuRef: ElementRef;
  @HostListener('document:click', ['$event'])
  clickout(event): void {
    if (!this.menuRef.nativeElement.contains(event.target)) {
      this.showMenu = false;
    }
  }

  constructor(private router: Router) {}

  ngOnInit(): void {}

  toggleMenu(): void {
    this.showMenu = !this.showMenu;
  }
}
