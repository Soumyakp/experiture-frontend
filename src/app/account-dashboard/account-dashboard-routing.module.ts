import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { AccountDashboardComponent } from './account-dashboard/account-dashboard.component';
import { LeadDashboardComponent } from './lead-dashboard/lead-dashboard.component';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'account',
        pathMatch: 'full'
      },
      {
        path: 'account',
        component: AccountDashboardComponent,
        data: { title: 'Dashboard | Account Dashboard' }
      },
      {
        path: 'lead',
        component: LeadDashboardComponent,
        data: { title: 'Dashboard | Lead Dashboard' }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountDashboardRoutingModule {}
