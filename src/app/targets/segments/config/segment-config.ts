export const SegmentColumns = [
  { field: 'editSegment', header: 'Edit Segment', sortable: false },
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'invalidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'validSMSCount', header: 'Deliverable SMS', sortable: true },
  {
    field: 'validPushNotificationsCount',
    header: 'Deliverable Push Notifications',
    sortable: true
  },
  { field: 'createdDate', header: 'Created Date', sortable: true },
  { field: 'deleteList', header: '', sortable: false },
  { field: 'associatePrograms', header: 'Associate Programs', sortable: false }
];

export const ConditionalSegColumns = [
  { field: 'editSegment', header: 'Edit Segment', sortable: false },
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'invalidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'validSMSCount', header: 'Deliverable SMS', sortable: true },
  {
    field: 'validPushNotificationsCount',
    header: 'Deliverable Push Notifications',
    sortable: true
  },
  { field: 'createdDate', header: 'Created Date', sortable: true },
  { field: 'deleteList', header: '', sortable: false },
  { field: 'associatePrograms', header: 'Associate Programs', sortable: true }
];
