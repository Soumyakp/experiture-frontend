import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CommonService } from 'src/app/shared/service/common.service';
import { environment } from 'src/environments/environment';

const API_URL = `${environment.API_URL}/api/Segment/`;

@Injectable({
  providedIn: 'root'
})
export class SegmentsService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID'); // 56058; // 56371
  constructor(private http: HttpClient, private commonService: CommonService) {}

  getSegments(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startIndex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramViews`,
      reqObj,
      this.httpOptions
    );
  }

  getSegCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramViewsTargetCount`,
      reqObj,
      this.httpOptions
    );
  }

  deleteSegment(): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      clientListId: 0,
      userId: +sessionStorage.getItem('USER_ID')
    };
    // return this.http.post(`${API_URL}`, reqObj, this.httpOptions);
    return of({ message: 'Api not ready yet!' });
  }

  exportData(item: any): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      clientListId: item.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(item.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(`${API_URL}GetViewSegmentExportedData`, reqObj, {
      responseType: 'arraybuffer'
    });
  }

  addToProgram(): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      userId: +sessionStorage.getItem('USER_ID'),
      listId: 0,
      isScheduleChecked: true,
      programId: 0,
      rdPickerScheduleTime: 'string',
      addTargets: 'string'
    };
    return this.http.post(`${API_URL}AddToPrograms`, reqObj, this.httpOptions);
  }

  getOutboundList(item: any): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      userId: +sessionStorage.getItem('USER_ID'),
      roleId: 0,
      agencyId: 0,
      userType: item.listSourceType
    };
    return this.http.post(
      `${API_URL}GetOutboundPrograms`,
      reqObj,
      this.httpOptions
    );
  }

  getConditionalSeg(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startIndex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}ConditionalSegment`,
      reqObj,
      this.httpOptions
    );
  }

  getConditionalSegCount(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText,
      startIndex,
      endIndex,
      sortExpression
    };
    return this.http.post(
      `${API_URL}ConditionalSegmentCount`,
      reqObj,
      this.httpOptions
    );
  }

  deleteConditionalSeg(clientListId: number): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      clientListId
    };
    // message: 'Api not working !'
    return this.http.post(
      `${API_URL}ConditionalSegmentDelete`,
      reqObj,
      this.httpOptions
    );
    // return of({ message: 'Api not ready yet!' });
  }

  exportConditionalData(item: any): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      clientListId: item.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(item.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(`${API_URL}ConditionalSegmentExportedData`, reqObj, {
      responseType: 'arraybuffer'
    });
  }
}
