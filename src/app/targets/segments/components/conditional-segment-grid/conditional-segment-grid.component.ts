import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { CommonService } from 'src/app/shared/service/common.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { TableUtilsService } from 'src/app/targets/lists/services/table-utils.service';
import { ConditionalSegColumns } from '../../config/segment-config';
import { SegmentsService } from '../../services/segments.service';

@Component({
  selector: 'app-conditional-segment-grid',
  templateUrl: './conditional-segment-grid.component.html',
  styleUrls: ['./conditional-segment-grid.component.scss']
})
export class ConditionalSegmentGridComponent implements OnInit {
  columns = ConditionalSegColumns;
  listData = [];
  targetCountLoading: boolean;
  targetCount: number;
  totalRecord: number;
  dbColumns = [];
  loading = true;
  editModal: boolean;
  recordDetails: any;
  deleteModal: boolean;

  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  exportModal: boolean;

  constructor(
    private segmentService: SegmentsService,
    private commonService: CommonService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService,
    private tableUtils: TableUtilsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  // getViews
  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.listData = this.tableUtils.resetDataSource(
        this.listData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }

    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.segmentService
      .getConditionalSeg(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          list.data.result.map(item => {
            if (
              item.listName.toLowerCase().trim() === 'inbound list' ||
              item.listName.toLowerCase().trim() === 'default list'
            ) {
              item.listSourceType = 'Inbound';
            }
            return item;
          });
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.segmentService
              .getConditionalSegCount(
                startIndex,
                endIndex,
                searchText,
                sortExpression
              )
              .subscribe(
                targetCount => {
                  this.targetCountLoading = false;
                  this.targetCount = targetCount.data.targetCount;
                },
                error => {
                  this.targetCountLoading = false;
                }
              );
          }
          if (this.paginator) {
            this.listData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.listData = this.tableUtils.updateDataSource(
              this.listData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.listData = [...this.listData];
            this.tRecords = this.listData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  private getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  showEditDialog(item: any, rowIndex: number): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/SegmentationNewList.aspx?src=rs&clstid=${item.clientListId}`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
    // else {
    //   this.router.navigate(['targets/segment/create']);
    // }
  }

  createSegmentHandler(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/SegmentationNewList.aspx?src=rs`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/segment/create']);
    }
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  gotoPage(page: any): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  showDeleteDialog(item: any, rowIndex: number): void {
    this.deleteModal = true;
    this.recordDetails = item;
  }

  deleteHandler(isConfirm: boolean): void {
    if (isConfirm) {
      this.deleteModal = false;
      this.loading = true;
      this.segmentService
        .deleteConditionalSeg(this.recordDetails.clientListId)
        .subscribe(
          res => {
            if (res.status === 'success') {
              this.toasterService.showSuccess('Segment removed successfully!');
              if (this.paginator) {
                this.onRefresh();
              } else {
                // this.table.clearCache();
                this.table.clearState();
                this.table.reset();
                this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
              }
            } else {
              this.loading = false;
            }
          },
          error => {
            this.loading = false;
          }
        );
    }
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.segmentService.exportConditionalData(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.segmentService
        .exportConditionalData(this.recordDetails)
        .subscribe(res => {
          this.toasterService.showExportSuccess(
            'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
          );
        });
    }
  }

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=rs`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSeg' }
      });
    }
  }
}
