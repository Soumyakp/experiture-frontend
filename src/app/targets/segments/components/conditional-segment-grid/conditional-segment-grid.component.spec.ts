import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConditionalSegmentGridComponent } from './conditional-segment-grid.component';

describe('ConditionalSegmentGridComponent', () => {
  let component: ConditionalSegmentGridComponent;
  let fixture: ComponentFixture<ConditionalSegmentGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionalSegmentGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionalSegmentGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
