import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddProgramDialogComponent } from './add-program-dialog.component';

describe('AddProgramDialogComponent', () => {
  let component: AddProgramDialogComponent;
  let fixture: ComponentFixture<AddProgramDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProgramDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProgramDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
