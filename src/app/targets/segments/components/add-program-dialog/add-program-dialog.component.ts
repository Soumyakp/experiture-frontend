import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/shared/service/common.service';
import { SegmentsService } from '../../services/segments.service';

interface ProgramList {
  programId: number;
  programName: string;
}

@Component({
  selector: 'app-add-program-dialog',
  templateUrl: './add-program-dialog.component.html',
  styleUrls: ['./add-program-dialog.component.scss']
})
export class AddProgramDialogComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  @Input() segDetails: any;
  header: string;
  programList: ProgramList[];
  // selectedProgram: ProgramList;
  selectedScheduler: string;
  showCalender: boolean;

  appendProgramForm: FormGroup = new FormGroup({
    programList: new FormControl(null, Validators.required),
    scheduler: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required),
    time: new FormControl(null, Validators.required)
  });
  loadingModal: boolean;
  time: NgbTimeStruct = { hour: 13, minute: 30, second: 0 };
  hourStep = 1;
  minuteStep = 45;
  iFrameSrc: string;

  constructor(
    private segmentsService: SegmentsService,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    if (this.commonService.getParamInfo()) {
      this.loadingModal = true;
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/AddTargetToProgram.aspx?Listid=${this.segDetails.clientListId}`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      this.iFrameSrc = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
      // window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.getOutboundProgram();
      this.header = `Append View ${this.segDetails.listName} to Program`;
    }
  }

  // For iFrame
  iFrameLoadEvent(): void {
    this.loadingModal = false;
  }

  iFrameClickHandler(event: any): void {
    console.log(event);
  }

  private getOutboundProgram(): void {
    this.loadingModal = true;
    this.segmentsService.getOutboundList(this.segDetails).subscribe(res => {
      setTimeout(() => {
        this.loadingModal = false;
        this.programList = [
          {
            programId: null,
            programName: 'Select Program'
          },
          ...res.data.result
        ];
        this.appendProgramForm.patchValue({
          programList: {
            programId: 71380,
            programName: '06032020Mandy'
          },
          scheduler: 'immediate'
        });
      }, 1000);
    });
  }

  hideDialog(event): void {
    this.visibleChange.emit(false);
  }

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  scheduleHandeler(): void {
    // console.log(this.appendProgramForm.get('scheduler').value);
    const rbVal = this.appendProgramForm.get('scheduler').value;
    if (rbVal === 'timer') {
      this.showCalender = true;
      const time: NgbTimeStruct = { hour: 12, minute: 0, second: 0 };
      this.appendProgramForm.patchValue({
        time
      });
    } else {
      this.showCalender = false;
    }
  }

  appendProgram(): void {
    console.log(this.appendProgramForm.value);
  }
}
