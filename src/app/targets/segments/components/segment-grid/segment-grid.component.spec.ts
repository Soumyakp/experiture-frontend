import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SegmentGridComponent } from './segment-grid.component';

describe('SegmentGridComponent', () => {
  let component: SegmentGridComponent;
  let fixture: ComponentFixture<SegmentGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SegmentGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
