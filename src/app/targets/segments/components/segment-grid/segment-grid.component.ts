import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { LazyLoadEvent, Message } from 'primeng/api';
import { Table } from 'primeng/table';
import { CommonService } from 'src/app/shared/service/common.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { TableUtilsService } from 'src/app/targets/lists/services/table-utils.service';
import { SegmentColumns } from '../../config/segment-config';
import { SegmentsService } from '../../services/segments.service';

@Component({
  selector: 'app-segment-grid',
  templateUrl: './segment-grid.component.html',
  styleUrls: ['./segment-grid.component.scss']
})
export class SegmentGridComponent implements OnInit {
  columns = SegmentColumns;
  listData = [];
  targetCountLoading: boolean;
  targetCount: number;
  totalRecord: number;
  dbColumns = [];
  loading = true;
  editModal: boolean;
  recordDetails: any;
  deleteModal: boolean;
  createSegModal: boolean;

  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  exportModal: boolean;
  msgs: Message[] = [];
  addProgramDialog: boolean;

  constructor(
    private segmentService: SegmentsService,
    private commonService: CommonService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService,
    private tableUtils: TableUtilsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  // getViews
  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.listData = this.tableUtils.resetDataSource(
        this.listData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }

    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.segmentService
      .getSegments(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          list.data.result.map(item => {
            if (
              item.listName.toLowerCase().trim() === 'inbound list' ||
              item.listName.toLowerCase().trim() === 'default list'
            ) {
              item.listSourceType = 'Inbound';
            }
            return item;
          });
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.segmentService.getSegCount(searchText).subscribe(
              targetCount => {
                this.targetCountLoading = false;
                this.targetCount = targetCount.data.targetCount;
              },
              error => {
                this.targetCountLoading = false;
              }
            );
          }
          if (this.paginator) {
            this.listData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.listData = this.tableUtils.updateDataSource(
              this.listData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.listData = [...this.listData];
            this.tRecords = this.listData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  private getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  showEditDialog(item: any, rowIndex: number): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/SegmentationNewList.aspx?clstid=${item.clientListId}`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
    // else {
    //   this.router.navigate(['targets/segment/create']);
    // }
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  gotoPage(page: any): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.segmentService.exportData(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.segmentService.exportData(this.recordDetails).subscribe(res => {
        this.toasterService.showExportSuccess(
          'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
        );
      });
    }
  }

  showModalDialog(item: any, rowIndex: number): void {
    this.deleteModal = true;
    this.recordDetails = item;
  }

  deletehandler(isDelete: boolean): void {
    if (isDelete) {
      // call delete api.
    }
  }

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=View`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSeg' }
      });
    }
  }

  addToProgram(item: any): void {
    this.addProgramDialog = true;
    this.recordDetails = item;
  }

  createSegmentHandler(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/SegmentationNewList.aspx`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/segment/create']);
    }
  }
}
