import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SegDashboardComponent } from './seg-dashboard.component';

describe('SegDashboardComponent', () => {
  let component: SegDashboardComponent;
  let fixture: ComponentFixture<SegDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
