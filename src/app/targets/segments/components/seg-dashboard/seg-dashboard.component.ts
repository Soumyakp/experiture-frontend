import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'app-seg-dashboard',
  templateUrl: './seg-dashboard.component.html',
  styleUrls: ['./seg-dashboard.component.scss']
})
export class SegDashboardComponent implements OnInit {
  showMenu: boolean;
  @ViewChild('menuRef') menuRef: ElementRef;
  @HostListener('document:click', ['$event'])
  clickout(event): void {
    if (!this.menuRef.nativeElement.contains(event.target)) {
      this.showMenu = false;
    }
  }

  constructor() {}

  ngOnInit(): void {}

  toggleMenu(): void {
    this.showMenu = !this.showMenu;
  }
}
