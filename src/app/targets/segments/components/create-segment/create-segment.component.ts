import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-segment',
  templateUrl: './create-segment.component.html',
  styleUrls: ['./create-segment.component.scss']
})
export class CreateSegmentComponent implements OnInit {
  constructor() {}

  ngOnInit() 
  {
    this.onExpandCollapse();
  }

  onExpandCollapse()
  {
      var coll = document.getElementsByClassName("acc_ctrl");
      var i;
      
      for (i = 0; i < coll.length; i++)
      {
          coll[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var content = this.nextElementSibling;
          if (content.style.display === "block")
          {
            content.classList.toggle("active");
            content.style.display = "none";
          } else {
            content.style.display = "block";
            content.classList.toggle("active");
          }
        });

      }
  }

  onChangeHeight(target:HTMLElement):void  {
    target.style.height= "100px";
  }

  onResetHeight(target:HTMLElement):void  {
    target.style.height= "18px";
  }
  
  
}