import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateSegmentComponent } from './create-segment.component';

describe('CreateSegmentComponent', () => {
  let component: CreateSegmentComponent;
  let fixture: ComponentFixture<CreateSegmentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSegmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSegmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
