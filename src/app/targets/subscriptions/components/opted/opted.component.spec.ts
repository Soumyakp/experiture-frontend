import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptedComponent } from './opted.component';

describe('OptedComponent', () => {
  let component: OptedComponent;
  let fixture: ComponentFixture<OptedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
