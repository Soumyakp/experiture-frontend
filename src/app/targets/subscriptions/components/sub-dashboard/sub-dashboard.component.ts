import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-sub-dashboard',
  templateUrl: './sub-dashboard.component.html',
  styleUrls: ['./sub-dashboard.component.scss']
})
export class SubDashboardComponent implements OnInit {
  showMenu: boolean;
  @ViewChild('menuRef') menuRef: ElementRef;
  @HostListener('document:click', ['$event'])
  clickout(event): void {
    if (!this.menuRef.nativeElement.contains(event.target)) {
      this.showMenu = false;
    }
  }

  constructor(private router: Router) {}

  ngOnInit(): void {}

  toggleMenu(): void {
    this.showMenu = !this.showMenu;
  }

  getTitle(): string {
    if (this.router.url.includes('subscription/optedout')) {
      return 'Opted Out Targets';
    } else if (this.router.url.includes('subscription/suppression')) {
      return 'Suppression List';
    } else if (this.router.url.includes('subscription/suppressed')) {
      return 'Suppressed Domains';
    }
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (this.router.url.includes('subscription/optedout')) {
          return 'Opted Out Targets';
        } else if (this.router.url.includes('subscription/suppression')) {
          return 'Suppression List';
        } else if (this.router.url.includes('subscription/suppressed')) {
          return 'Suppressed Domains';
        }
      });
  }
}
