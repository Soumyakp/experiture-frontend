import { ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild } from '@angular/core';
import { OptedOutColumns } from '../../config/subscription-config';
import { OptedService } from '../../services/opted.service';

@Component({
  selector: 'app-suppressed',
  templateUrl: './suppressed.component.html',
  styleUrls: ['./suppressed.component.scss']
})
export class SuppressedComponent implements OnInit {

  columns = OptedOutColumns;
  listData = [];
  targetCountLoading: boolean;
  targetCount: number;
  totalRecord: number;
  dbColumns = [];
  loading = true;
  editModal: boolean;
  recordDetails: any;
  unsuppressedModal: boolean;
  createSegModal: boolean;
  selectedTarget = [];
  searchFields: any;
  selectedField: any;

 
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  exportModal: boolean;


  constructor(
    private optedService: OptedService,
    
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;

    this.optedService.unsubscribeDomain().subscribe(res => {
      console.log(res);
    });
  }

  gotoPage(page: any): void {
    
  }

}
