import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppressedComponent } from './suppressed.component';

describe('SuppressedComponent', () => {
  let component: SuppressedComponent;
  let fixture: ComponentFixture<SuppressedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuppressedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppressedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
