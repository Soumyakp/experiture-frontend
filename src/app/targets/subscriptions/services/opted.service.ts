import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from 'src/app/shared/service/common.service';
import { environment } from 'src/environments/environment';

const API_URL = `${environment.API_URL}/api/Subscriptions/`;

@Injectable({
  providedIn: 'root'
})
export class OptedService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID');

  constructor(private http: HttpClient, private commonService: CommonService) {}

  getOptedList(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startIndex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramViews`,
      reqObj,
      this.httpOptions
    );
  }

  getOptedCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramViewsTargetCount`,
      reqObj,
      this.httpOptions
    );
  }

  exportData(item: any): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      clientListId: item.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(item.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(`${API_URL}GetViewSegmentExportedData`, reqObj, {
      responseType: 'arraybuffer'
    });
  }

  unsubscribeDomain(): Observable<any> {
    return this.http.post(
      `${API_URL}UnsubscribedDomains`,
      { clientId: this.clientId },
      this.httpOptions
    );
  }
}
