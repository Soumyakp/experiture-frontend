import { TestBed } from '@angular/core/testing';

import { OptedService } from './opted.service';

describe('OptedService', () => {
  let service: OptedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OptedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
