export const OptedOutColumns = [
  { field: 'program', header: 'Program', sortable: true },
  { field: 'campaignName', header: 'Campaign Name', sortable: true },
  { field: 'emailAddress', header: 'Email Address', sortable: true },
  { field: 'unsubscribeDate', header: 'Unsubscribe Date', sortable: true },
  { field: 'category', header: 'Category', sortable: true }
];
