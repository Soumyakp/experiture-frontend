import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/shared/service/common.service';

const API_URL = `${environment.API_URL}/api/List/`;

@Injectable({
  providedIn: 'root'
})
export class CrmListService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID');

  constructor(private http: HttpClient, private commonService: CommonService) {}

  getCrmList(
    startIndex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startindex = startIndex === 0 ? 0 : startIndex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startindex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramCrmList`,
      reqObj,
      this.httpOptions
    );
  }

  getCrmTargetCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramCrmListCount`,
      reqObj,
      this.httpOptions
    );
  }

  editList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}GetSourceForProgramCrmEdit`,
      reqBody,
      this.httpOptions
    );
  }

  exportCrm(reqBody: any): Observable<any> {
    const reqObj = {
      clientId: reqBody.clientId,
      clientListId: reqBody.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(reqBody.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramCrmExportedData`,
      reqObj,
      { responseType: 'arraybuffer' }
    );
  }
}
