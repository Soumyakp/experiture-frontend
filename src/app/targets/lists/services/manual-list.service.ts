import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CommonService } from 'src/app/shared/service/common.service';
import { tap } from 'rxjs/operators';

const API_URL = `${environment.API_URL}/api/List/`;

@Injectable({
  providedIn: 'root'
})
export class ManualListService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID'); // 56058; // 56371
  constructor(private http: HttpClient, private commonService: CommonService) {}

  getProgramList(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startIndex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http
      .post(`${API_URL}GetSourceForProgramList`, reqObj, this.httpOptions)
      .pipe(
        tap(n => {
          this.getProgramListCount().subscribe(res => {});
        })
      );
  }

  getProgramListCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramListCount`,
      reqObj,
      this.httpOptions
    );
  }

  editList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramListEdit`,
      reqBody,
      this.httpOptions
    );
  }

  exportList(reqBody: any): Observable<any> {
    const reqObj = {
      clientId: reqBody.clientId,
      clientListId: reqBody.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(reqBody.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramListExportedData`,
      reqObj,
      { responseType: 'arraybuffer' }
    );
  }

  deleteList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramListDelete`,
      reqBody,
      this.httpOptions
    );
  }

  // getDestination(reqBody = {}): Observable<any> {
  //   const reqObj = {
  //     clientId: this.clientId
  //   };
  //   return this.http.post(
  //     `${API_URL}GetTableListByClientId`,
  //     reqObj,
  //     this.httpOptions
  //   );
  // }

  // createTarget(reqBody: any): Observable<any> {
  //   const reqObj = {
  //     ...reqBody,
  //     clientId: this.clientId,
  //     userId: +sessionStorage.getItem('USER_ID'),
  //     connectionStringPrams: ''

  //     // listSource: 0,
  //     // listFilePath: '',
  //     // listYear: 0,
  //     // schemaId: 0,
  //     // createdDate: new Date(),
  //     // listDataSource: 0,
  //     // file: undefined,
  //     // listId: '',
  //     // campListId: '',
  //     // isFtp: '',
  //     // fileType: '',
  //     // fileName: '',
  //     // fileSize: '',
  //     // listAppendId: 0,
  //     // editionId: 0
  //   };
  //   return this.http.post(
  //     `${API_URL}SourceForProgramListCreateTarget`,
  //     reqObj,
  //     this.httpOptions
  //   );
  // }

  // targetUpload(data: any, file: any): Observable<any> {
  //   const formData = new FormData();
  //   formData.append('ClientId', '' + this.clientId);
  //   formData.append('IsFtp', 'false');
  //   formData.append('ListName', data.listName);
  //   formData.append('ListDescription', data.listDescription);
  //   formData.append('ListDataSource', '0');
  //   formData.append('UserId', sessionStorage.getItem('USER_ID'));
  //   formData.append('RelationalObjectId', data.relationalObjectId);
  //   formData.append('File', file);
  //   formData.append('ListId', data.listId);
  //   formData.append('CampListId', '');
  //   formData.append('ConnectionStringPrams', '');

  //   return this.http.post(
  //     `${API_URL}SourceForProgramListCreateTargetUpload`,
  //     formData
  //   );
  // }
}
