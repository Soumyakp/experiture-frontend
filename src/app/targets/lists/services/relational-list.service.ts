import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CommonService } from 'src/app/shared/service/common.service';

const API_URL = `${environment.API_URL}/api/List/`;

@Injectable({
  providedIn: 'root'
})
export class RelationalListService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID');

  constructor(private http: HttpClient, private commonService: CommonService) {}

  getRelationList(
    startIndex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startindex = startIndex === 0 ? 0 : startIndex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startindex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramListRelational`,
      reqObj,
      this.httpOptions
    );
  }

  getRelationTargetCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramListRelationalTargetCount`,
      reqObj,
      this.httpOptions
    );
  }

  editList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramListRelationalEdit`,
      reqBody,
      this.httpOptions
    );
  }

  exportList(reqBody: any): Observable<any> {
    const reqObj = {
      clientId: reqBody.clientId,
      clientListId: reqBody.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(reqBody.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramListRelationalExportedData`,
      reqObj,
      { responseType: 'arraybuffer' }
    );
  }

  deleteList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramListRelationalDelete`,
      reqBody,
      this.httpOptions
    );
  }

  getRelationSftpList(
    startIndex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Created_Date desc'
  ): Observable<any> {
    const startindex = startIndex === 0 ? 0 : startIndex + 1;
    const reqObj = {
      programId: 0,
      campaignId: 0,
      clientId: this.clientId,
      startindex,
      endIndex,
      searchField: '',
      searchText,
      sortExpression
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramFtpRelational`,
      reqObj,
      this.httpOptions
    );
  }

  getRelationSftpTargetCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      programId: 0,
      campaignId: 0,
      searchField: '',
      searchText
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramFtpRelationalTargetCount`,
      reqObj,
      this.httpOptions
    );
  }

  exportFtpList(reqBody: any): Observable<any> {
    const reqObj = {
      clientId: reqBody.clientId,
      clientListId: reqBody.clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      exportFileName: this.commonService.getExportFileName(reqBody.listName),
      exportFileType: 'CSV'
    };
    return this.http.post(
      `${API_URL}GetSourceForProgramFtpRelationalExportedData`,
      reqObj,
      { responseType: 'arraybuffer' }
    );
  }

  deleteSftpList(reqBody: any): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramFtpRelationalDelete`,
      reqBody,
      this.httpOptions
    );
  }
}
