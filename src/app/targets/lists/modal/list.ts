export interface List {
  bounceCount: number;
  clientId: number;
  clientListId: number;
  createdDate: string;
  deliverableCount: number;
  id: number;
  inValidEmailCount: number;
  isActive: boolean;
  isDynamic: boolean;
  listDataSource: string;
  listDescription: string;
  listFilePath: string;
  listName: string;
  listRecordCount: number;
  listSource: string;
  listSourceId: number;
  listSourceType: string;
  listStep: number;
  listYear: number;
  mergeOption: string;
  programId: number;
  relationalObjectId: number;
  sequenceOrder: number;
  socialType: string;
  socialUserId: string;
  tempTableName: string;
  unsubscribeCount: number;
  validEmailCount: number;
  validPushNotificationsCount: number;
  validSMSCount: number;
}

export interface ProgramListRes {
  data: Data;
  message: string;
  status: string;
}

export interface Columns {
  columnName: string;
  dbColumnName: string;
}

export interface Data {
  result: List[];
  columns: Columns[];
  startIndex: number;
  endIndex: number;
  totalCount: number;
}

export interface ProgramListCountRes {
  data: { targetCount: number };
  message: string;
  status: string;
}

export interface ProgramListReq {
  campaignId: number;
  clientId: number;
  endIndex: number;
  programId: number;
  searchField: string;
  searchText: string;
  sortExpression: string;
  startindex: number;
}

export interface ProgramListCountReq {
  campaignId: number;
  clientId: number;
  programId: number;
  searchField: string;
  searchText: string;
}
