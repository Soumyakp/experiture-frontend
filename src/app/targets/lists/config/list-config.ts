export const ManualColumns = [
  { field: 'addRecord', header: 'Add Records', sortable: false },
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'editList', header: '', sortable: false },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'inValidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'validSMSCount', header: 'Deliverable SMS', sortable: true },
  {
    field: 'validPushNotificationsCount',
    header: 'Deliverable Push Notifications',
    sortable: true
  },
  { field: 'createdDate', header: 'Created Date', sortable: true },
  { field: 'deleteList', header: '', sortable: false }
];

export const InboundColumns = [
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'invalidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'validSMSCount', header: 'Deliverable SMS', sortable: true },
  {
    field: 'validPushNotificationsCount',
    header: 'Deliverable Push Notifications',
    sortable: true
  },
  { field: 'createdDate', header: 'Created Date', sortable: true }
];

export const SFTPColumns = [
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'editList', header: '', sortable: false },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'inValidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'createdDate', header: 'Created Date', sortable: true },
  { field: 'deleteList', header: '', sortable: false }
];

export const CRMColumns = [
  { field: 'addRecord', header: 'CRM Records', sortable: false },
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'editList', header: '', sortable: false },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'validEmailCount', header: 'Valid Emails', sortable: true },
  {
    field: 'inValidEmailCount',
    header: 'Invalid/ Unavailable',
    sortable: true
  },
  { field: 'deliverableCount', header: 'Deliverable', sortable: true },
  { field: 'bounceCount', header: 'Bounces', sortable: true },
  { field: 'unsubscribeCount', header: 'Unsubscribes', sortable: true },
  { field: 'createdDate', header: 'Created Date', sortable: true }
];

export const RelationalColumns = [
  { field: 'listName', header: 'Name', sortable: true },
  { field: 'editList', header: '', sortable: false },
  { field: 'listSourceType', header: 'Type', sortable: true },
  { field: 'relationalObject', header: 'Relational Object', sortable: true },
  { field: 'listRecordCount', header: 'Record Count', sortable: true },
  { field: 'viewList', header: '', sortable: false },
  { field: 'export', header: 'Export', sortable: false },
  { field: 'createdDate', header: 'Created Date', sortable: true },
  { field: 'deleteList', header: '', sortable: false }
];
