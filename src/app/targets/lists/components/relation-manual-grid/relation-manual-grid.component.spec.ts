import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RelationManualGridComponent } from './relation-manual-grid.component';

describe('RelationManualGridComponent', () => {
  let component: RelationManualGridComponent;
  let fixture: ComponentFixture<RelationManualGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RelationManualGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationManualGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
