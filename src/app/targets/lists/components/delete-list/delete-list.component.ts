import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-delete-list',
  templateUrl: './delete-list.component.html',
  styleUrls: ['./delete-list.component.scss']
})
export class DeleteListComponent implements OnInit {
  @Output() deleteItem = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  onDeleteList(): void {
    this.deleteItem.emit(true);
  }

  onDeny(): void {
    this.deleteItem.emit(false);
  }
}
