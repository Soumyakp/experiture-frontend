import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExportNotificationComponent } from './export-notification.component';

describe('ExportNotificationComponent', () => {
  let component: ExportNotificationComponent;
  let fixture: ComponentFixture<ExportNotificationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
