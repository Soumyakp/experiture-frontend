import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/service/common.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';

@Component({
  selector: 'app-export-notification',
  templateUrl: './export-notification.component.html',
  styleUrls: ['./export-notification.component.scss']
})
export class ExportNotificationComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  emailForm = new FormGroup({
    emailAddress: new FormControl(null, [
      Validators.required,
      Validators.email
    ]),
    isRemember: new FormControl(false)
  });
  @Output() exporthandler = new EventEmitter();
  isDisabled: boolean;
  loading: boolean;

  constructor(
    private commonService: CommonService,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.getSavedEmail();
  }

  private getSavedEmail(): void {
    this.loading = true;
    this.commonService.getExportNotificationEmail().subscribe(
      res => {
        if (res.data.isRemember) {
          this.emailForm.patchValue({
            emailAddress: res.data.email,
            isRemember: true
          });
          this.isDisabled = true;
        }
        this.loading = false;
      },
      error => {
        this.loading = false;
      }
    );
  }

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  onOkay(): void {
    this.loading = true;
    this.commonService
      .saveExportNotificationEmail(this.emailForm.value)
      .subscribe(
        res => {
          this.loading = false;
          this.visible = false;
          this.exporthandler.emit(true);
          this.toasterService.showExportSuccess(
            'Export Notification settings saved successfully.'
          );
        },
        error => {
          this.loading = false;
          this.visible = false;
          this.exporthandler.emit(false);
        }
      );
  }

  onEdit(): void {
    this.isDisabled = false;
    this.emailForm.patchValue({
      isRemember: false
    });
  }
}
