import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { LazyLoadEvent } from 'primeng/api';

import { ManualListService } from '../../services/manual-list.service';
import { CommonService } from '../../../../shared/service/common.service';
import { ManualColumns } from '../../config/list-config';
import { List } from '../edit-list/edit-list.component';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { TableUtilsService } from '../../services/table-utils.service';

@Component({
  selector: 'app-manual-grid',
  templateUrl: './manual-grid.component.html',
  styleUrls: ['./manual-grid.component.scss']
})
export class ManualGridComponent implements OnInit, AfterViewChecked {
  columns = ManualColumns;
  listData = [];
  targetCountLoading: boolean;
  targetCount: number;
  totalRecord: number;
  dbColumns = [];
  loading = true;
  editModal: boolean;
  recordDetails: any;
  deleteModal: boolean;
  addRecordModal: boolean;
  addListModal: boolean;

  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  exportModal: boolean;

  constructor(
    private manualService: ManualListService,
    private commonService: CommonService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService,
    private tableUtils: TableUtilsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  ngAfterViewChecked(): void {}

  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.listData = this.tableUtils.resetDataSource(
        this.listData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }

    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.manualService
      .getProgramList(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          list.data.result.map(item => {
            if (
              item.listName.toLowerCase().trim() === 'inbound list' ||
              item.listName.toLowerCase().trim() === 'default list'
            ) {
              item.listSourceType = 'Inbound';
            }
            return item;
          });
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.manualService.getProgramListCount(searchText).subscribe(
              targetCount => {
                this.targetCountLoading = false;
                this.targetCount = targetCount.data.targetCount;
              },
              error => {
                this.targetCountLoading = false;
              }
            );
          }
          if (this.paginator) {
            this.listData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.listData = this.tableUtils.updateDataSource(
              this.listData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.listData = [...this.listData];
            this.tRecords = this.listData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  gotoPage(page: string): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  reloadGridHandler(): void {
    this.resetPage();
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  private getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  showEditDialog(item: any, rowIndex: number): void {
    this.editModal = true;
    this.recordDetails = { rowIndex, ...item };
  }

  onListEdit(list: List): void {
    this.editModal = false;
    if (
      this.recordDetails.listName === list.listName &&
      this.recordDetails.listDescription === list.listDescription
    ) {
      this.toasterService.showInformation('No changes made!');
    } else {
      const listData = {
        listSource: this.recordDetails.listSourceId,
        listYear: this.recordDetails.listYear,
        listId: this.recordDetails.clientListId,
        ...list
      };
      this.loading = true;
      this.manualService.editList(listData).subscribe(
        res => {
          if (res.status === 'success') {
            this.toasterService.showSuccess('List updated successfully!');
            if (this.paginator) {
              this.onRefresh();
            } else {
              // this.table.clearCache();
              this.table.clearState();
              this.table.reset();
              this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
            }
          } else {
            this.loading = false;
          }
        },
        error => {
          this.loading = false;
        }
      );
    }
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.manualService.exportList(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.manualService.exportList(this.recordDetails).subscribe(res => {
        this.toasterService.showExportSuccess(
          'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
        );
      });
    }
  }

  confirm(item: any, rowIndex: number): void {
    this.deleteModal = true;
    this.recordDetails = { rowIndex, ...item };
  }

  onDeleteList(isConfirm: boolean): void {
    if (isConfirm) {
      const listData = {
        clientListId: this.recordDetails.clientListId,
        clientId: this.recordDetails.clientId
      };
      this.deleteModal = false;
      this.loading = true;
      this.manualService.deleteList(listData).subscribe(
        res => {
          if (res.status === 'success') {
            this.toasterService.showSuccess('List removed successfully!');
            if (this.paginator) {
              this.onRefresh();
            } else {
              // this.table.clearCache();
              this.table.clearState();
              this.table.reset();
              this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
            }
          } else {
            this.loading = false;
          }
        },
        error => {
          this.loading = false;
        }
      );
    }
  }

  addRecord(item: any): void {
    this.addRecordModal = true;
    this.recordDetails = item;
  }

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=List`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSource' },
        fragment: 'manual'
      });
    }
  }
}
