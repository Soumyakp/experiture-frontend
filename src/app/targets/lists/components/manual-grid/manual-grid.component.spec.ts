import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ManualGridComponent } from './manual-grid.component';

describe('ManualGridComponent', () => {
  let component: ManualGridComponent;
  let fixture: ComponentFixture<ManualGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
