import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  DoCheck
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval } from 'rxjs';
import { CommonService } from 'src/app/shared/service/common.service';
import { ManualListService } from '../../services/manual-list.service';
import { AddListService, ListDetails } from './add-list.service';
import { ProcessingFileRequest } from './add-list.service';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss']
})
export class AddListComponent implements OnInit {
  @Input() visible: boolean;
  header = 'Add a List or CRM Connection';
  showDefault = true;
  showCreateList: boolean;
  showUploadFile: boolean;
  showProcessingFile: boolean;
  showMapTable: boolean;
  showProcessingList: boolean;
  showAppending: boolean;
  showSFTPCreateList: boolean;
  showFrequency: boolean;

  loading: boolean;
  destinationData = [];
  selectedType: any;
  progressValue: number;
  mailChecked = [];
  @Output() visibleChange = new EventEmitter();
  @Output() reloadGrid = new EventEmitter();
  iFrameSrc: string;
  loadingModal: boolean;

  constructor(
    private manualService: ManualListService,
    private changeDetectorRef: ChangeDetectorRef,
    private commonService: CommonService,
    private addListService: AddListService
  ) {}

  // ngDoCheck(): void {
  //   if (this.selectedMain === 'skip') {
  //     this.selectedChild = undefined;
  //   }
  //   if (this.selectedChild) {
  //     this.selectedMain = 'update';
  //   }
  //   if (this.selectedSubChild?.length > 0) {
  //     this.selectedSub = undefined;
  //   }
  // }

  ngOnInit(): void {
    if (this.commonService.getParamInfo()) {
      this.loadingModal = true;
      const encObj = {
        ...this.commonService.getParamInfo(),
        // userName: 'experituresales@gmail.com',
        // password: 'Test@123',
        // rootUserId: 10701,
        redirect: `/List/SelectDataSourceType.aspx`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      this.iFrameSrc = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
      // window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  // For iFrame
  iFrameLoadEvent(): void {
    this.loadingModal = false;
  }

  iFrameToggle(event: any): void {
    console.log(event);
  }

  private getDestinationData(): void {
    this.loading = true;
    this.addListService.getDestination().subscribe(res => {
      this.destinationData = res.data.result;
      this.loading = false;
      this.showDefault = false;
      this.showCreateList = true;
    });
  }

  createTagret(): void {
    this.header = 'Create/Upload New List';
    this.addListService.listDetails = {
      sourceData: {
        creationType: 'Manual',
        buttonLabel: 'Create'
      }
    };
    this.getDestinationData();
  }

  createFTPTarget(): void {
    this.header = 'Create a SFTP/FTP Target List';
    this.addListService.listDetails = {
      sourceData: {
        creationType: 'SFTP',
        buttonLabel: 'Next'
      }
    };
    this.getDestinationData();
  }

  hideDialog(event): void {
    this.visibleChange.emit(false);
    console.log('hide', this.addListService.listDetails);
    if (this.addListService.listDetails.completed) {
      this.reloadGrid.emit();
    }
  }

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
    console.log('hide', this.addListService.listDetails);
    if (this.addListService.listDetails.completed) {
      this.reloadGrid.emit();
    }
  }

  listCreated(listData: any): void {
    if (this.addListService.listDetails.sourceData.creationType === 'Manual') {
      // this.listDetails = listData;
      this.header = 'Upload a Target List from file';
      this.showCreateList = false;
      this.showUploadFile = true;
    } else if (
      this.addListService.listDetails.sourceData.creationType === 'SFTP'
    ) {
      // this.listDetails = listData;
      this.addListService.listDetails.sourceData = {
        creationType: 'SFTP',
        buttonLabel: 'Next'
      };
      this.showCreateList = false;
      this.showSFTPCreateList = true;
    }
  }

  listUploaded(upData: any): void {
    this.showUploadFile = false;

    this.showMapTable = true;
    this.header = 'Map Fields';
    // this.listDetails = {
    //   ...this.listDetails,
    //   ...upData.value
    // };
  }

  backToCreateList(prevPageInfo: any): void {
    this.addListService.listDetails.sourceData = {
      creationType: 'SFTP',
      buttonLabel: 'Next',
      prevPageInfo
    };
    this.showCreateList = true;
    this.showSFTPCreateList = false;
  }

  listSave(event: any): void {
    this.header = 'Precessing List';
    this.showMapTable = false;
    this.showProcessingList = true;
    if (this.addListService.listDetails.sourceData.creationType === 'Manual') {
      this.addListService.listDetails.sourceData = {
        creationType: 'Manual',
        buttonLabel: 'Save & Continue'
      };
    } else if (
      this.addListService.listDetails.sourceData.creationType === 'SFTP'
    ) {
      this.addListService.listDetails.sourceData = {
        creationType: 'SFTP',
        buttonLabel: 'Save & Close'
      };
      // this.visibleChange.emit(false);
    }
  }

  // allCategoryCheck(event): void {
  //   if (event.checked) {
  //     this.selectedSubChild = undefined;
  //   }
  // }

  listProcessed(proData: any): void {
    if (this.addListService.listDetails.sourceData.creationType === 'Manual') {
      this.header = 'Appending Data';
      this.showProcessingList = false;
      this.showAppending = true;
    } else if (
      this.addListService.listDetails.sourceData.creationType === 'SFTP'
    ) {
      // this.pageInfo = {
      //   pageName: 'SFTP',
      //   buttonLabel: 'Save & Close'
      // };
      this.visibleChange.emit(false);
    }
  }

  uploadCompleted(): void {
    this.visible = false;
    this.reloadGrid.emit(true);
  }

  ftpListCreated(data: any): void {
    console.log('ftp data', data);
    this.showSFTPCreateList = false;
    this.showFrequency = true;
  }

  frequencySet(data: any): void {
    console.log('ftp data', data);
    this.header = 'Upload a Target List from file';
    this.showFrequency = false;
    this.showUploadFile = true;
  }
}
