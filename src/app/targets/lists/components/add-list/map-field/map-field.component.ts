import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AddListService } from '../add-list.service';

@Component({
  selector: 'app-map-field',
  templateUrl: './map-field.component.html',
  styleUrls: ['./map-field.component.scss']
})
export class MapFieldComponent implements OnInit {
  // @Input() pageInfo: any;
  // @Input() listDetails: any;
  @Output() listSaved = new EventEmitter();
  @Output() denyModal = new EventEmitter();
  items = [];
  expOption = [];
  loading: boolean;

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {
    this.getMapping();
  }

  private getMapping(): void {
    this.loading = true;
    forkJoin({
      expField: this.addListService.getExperitureFields(),
      excel: this.addListService.getMappingValues()
    }).subscribe(({ excel, expField }) => {
      this.loading = false;
      this.expOption = expField.data.map(item => {
        return {
          label: item.value,
          key: item.key,
          inactive: item.key === 2 || item.key === 20
        };
      });
      this.items = excel.data.map(item => {
        const fieldOption = this.expOption.find(
          val => val.label === item.listFields
        );
        return {
          ...item,
          fieldId: fieldOption ? fieldOption : this.expOption[0],
          showNewCustomField: false,
          newCustomFieldValue: null,
          isCustomFieldRequired: false
        };
      });
    });
  }

  expFieldChange(event: any, item: any): void {
    if (item.fieldId) {
      if (item.fieldId.key === 1) {
        item.showNewCustomField = true;
        item.newCustomFieldValue = item.listFields;
      } else {
        item.showNewCustomField = false;
        item.newCustomFieldValue = null;
        item.isCustomFieldRequired = false;
      }
    } else {
      item.showNewCustomField = false;
      item.newCustomFieldValue = null;
      item.isCustomFieldRequired = false;
    }
  }

  saveAndUpload(): void {
    console.log('this.items', this.items);
    const isValid: boolean[] = [];
    this.items.forEach(item => {
      if (item.fieldId.key === 1) {
        if (!item.newCustomFieldValue) {
          item.isCustomFieldRequired = true;
          isValid.push(false);
        } else {
          item.isCustomFieldRequired = false;
          isValid.push(true);
        }
      }
    });
    if (isValid.every(e => e)) {
      const fields = this.items.map(item => {
        return {
          listFields: item.listFields,
          fieldName:
            item.fieldId.key === 1
              ? item.newCustomFieldValue
              : item.fieldId.label,
          dateFormat: 'DD/MM/YYYY'
        };
      });
      this.saveMappingFields(fields);
    }
    // this.saveMappingFields();

    // this.showMapTable = false;
    // this.header = 'Precessing List';
    // this.showProcessingList = true;
  }

  private saveMappingFields(fields: any): void {
    this.addListService.saveMappingFields(fields).subscribe(res => {
      this.addListService.listDetails = {
        ...this.addListService.listDetails,
        uploadId: +res.data.uploadId
      };
      this.listSaved.emit({
        value: res.data
      });
    });
  }

  deny(): void {
    this.denyModal.emit(true);
  }
}
