import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface ListDetails {
  ac?: number;
  campListId?: number;
  completed?: boolean;
  cc?: number;
  sourceData?: SourceData;
  isFtp?: string;
  isProcessOn?: boolean;
  isUpload?: boolean;
  listDescription?: string;
  listId?: number;
  listName?: string;
  relationalObjectId?: number;
  relationalObjectName?: string;
  tc?: number;
  uploadFileType?: string;
  uploadId?: number;
  uploadedCSVFileName?: string;
  uploadedNewListAppendId?: number;
}

export interface SourceData {
  creationType?: string;
  buttonLabel?: string;
  prevPageInfo?: any;
}

export interface CreateTargetResponse {
  data: CreateTargetResponseData;
  message: string;
  status: string;
}

export interface CreateTargetResponseData {
  listId: string;
  relationalObjectId: string;
  relationalObjectName: string;
}

export interface TargetUploadResponse {
  data: TargetUploadResponseData;
  message: string;
  status: string;
}

export interface TargetUploadResponseData {
  isProcessOn: boolean;
  isUpload: boolean;
  uploadedCSVFileName: string;
  uploadedNewListAppendId: number;
}

export interface ProcessingFileRequest {
  clientId?: number;
  userId?: number;
  relationalObjectId: string;
  listId: string;
  campListId?: string;
  isFtp: string;
  fileType: string;
  fileName: string;
  fileSize: string;
  listAppendId: number;
  editionId: number;
  connectionStringPrams: string;
}

const API_URL = `${environment.API_URL}/api/List/`;

@Injectable({
  providedIn: 'root'
})
export class AddListService {
  listDetails: ListDetails = {
    completed: false
  };
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID');

  constructor(private http: HttpClient) {}

  getDestination(reqBody = {}): Observable<any> {
    const reqObj = {
      clientId: this.clientId
    };
    return this.http.post(
      `${API_URL}GetTableListByClientId`,
      reqObj,
      this.httpOptions
    );
  }

  createTarget(reqBody: any): Observable<any> {
    // const { pageName, ...rest } = reqBody;
    // if (pageName === 'Manual') {
    const reqObj = {
      clientId: this.clientId,
      listName: reqBody.listName,
      listDescription: reqBody.listDescription,
      userId: +sessionStorage.getItem('USER_ID'),
      relationalObjectName: reqBody.relationalObjectName,
      relationalObjectId: reqBody.relationalObjId,
      connectionStringPrams: ''
    };
    return this.http.post(
      `${API_URL}SourceForProgramListCreateTarget`,
      reqObj,
      this.httpOptions
    );
    // }
  }

  targetUpload(file: any): Observable<any> {
    console.log('he', this.listDetails);
    const formData = new FormData();
    formData.append('ClientId', sessionStorage.getItem('CLIENT_ID'));
    formData.append('IsFtp', 'false');
    formData.append('ListName', this.listDetails.listName);
    formData.append('ListDescription', this.listDetails.listDescription);
    formData.append('ListDataSource', '0');
    formData.append('UserId', sessionStorage.getItem('USER_ID'));
    formData.append(
      'RelationalObjectId',
      this.listDetails.relationalObjectId.toString()
    );
    formData.append('File', file);
    formData.append('ListId', this.listDetails.listId.toString());
    formData.append('CampListId', '');
    formData.append('ConnectionStringPrams', '');

    return this.http.post(
      `${API_URL}SourceForProgramListCreateTargetUpload`,
      formData
    );
  }

  processingFile(reqBody: ProcessingFileRequest): Observable<any> {
    const reqPayload = {
      clientId: this.clientId,
      userId: +sessionStorage.getItem('USER_ID'),
      relationalObjectId: reqBody.relationalObjectId,
      listId: reqBody.listId,
      campListId: '',
      isFtp: reqBody.isFtp,
      fileType: reqBody.fileType,
      fileName: reqBody.fileName,
      fileSize: reqBody.fileSize,
      listAppendId: reqBody.listAppendId,
      editionId: reqBody.editionId,
      connectionStringPrams: reqBody.connectionStringPrams
    };
    return this.http.post(
      `${API_URL}SourceForProgramListCreateTargetProcessingFile`,
      reqPayload,
      this.httpOptions
    );
  }

  /**
   * Get the mapping details
   * @param data type of any(TODO: Need to provide type)
   */
  getMappingValues(): Observable<any> {
    const reqPayload = {
      clientId: this.clientId,
      listId: this.listDetails.listId,
      relationalObjectId: '' + this.listDetails.relationalObjectId,
      arrFieldLists: [],
      mainListId: this.listDetails.uploadedNewListAppendId,
      connectionStringPrams: ''
    };
    return this.http.post(
      `${API_URL}SourceForProgramListCreateTargetGetMappingValues`,
      reqPayload,
      this.httpOptions
    );
  }

  getExperitureFields(): Observable<any> {
    return this.http.post(
      `${API_URL}SourceForProgramListCreateTargetGetMappingValuesDropdown`,
      { clientId: this.clientId, connectionStringPrams: '' },
      this.httpOptions
    );
  }

  saveMappingFields(fieldMappings: any): Observable<any> {
    const reqPayload = {
      clientId: this.clientId,
      relationalObjId: this.listDetails.relationalObjectId.toString(),
      fieldMappings,
      crmSource: '10', // need check later
      marketingChannelId: 0, // need check later
      listId: this.listDetails.listId,
      mainListId: this.listDetails.uploadedNewListAppendId.toString(),
      editionId: 0,
      userId: +sessionStorage.getItem('USER_ID'),
      campaignId: 0,
      isFtp: '',
      fileName: this.listDetails.uploadedCSVFileName, // need this
      fileType: this.listDetails.uploadFileType // need this
    };
    return this.http.post(
      `${API_URL}SourceForProgramListCreateTargetUpdateMappingAppendList`,
      reqPayload,
      this.httpOptions
    );
  }

  // updateMappingAppendList(data: any): Observable<any> {
  //   const reqPayload = {
  //     clientId: this.clientId,
  //     relationalObjId: data.relationalObjectId,
  //     fieldMappings: [
  //       {
  //         listFields: 0,
  //         fieldName: 'string',
  //         dateFormat: 'string'
  //       }
  //     ],
  //     crmSource: 'string',
  //     marketingChannelId: 0,
  //     listId: data.listId,
  //     profields: [{}],
  //     mainListId: 'string',
  //     editionId: 0,
  //     userId: 0,
  //     campaignId: 0,
  //     isFtp: 'string',
  //     fileName: 'string',
  //     fileType: 'string'
  //   };
  //   return this.http.post(
  //     `${API_URL}SourceForProgramListCreateTargetUpdateMappingAppendList`,
  //     reqPayload,
  //     this.httpOptions
  //   );
  // }

  /**
   * Skip duplicate section
   */
  emailCategoryList(): Observable<any> {
    return this.http.post(
      `${API_URL}PopulateEmailCategoryList`,
      { clientId: this.clientId },
      this.httpOptions
    );
  }

  saveCategorySetting(reqObj: any): Observable<any> {
    const reqPayload = {
      ...reqObj,
      campListId: this.listDetails.campListId,
      uploadId: this.listDetails.uploadId,
      isFtp: this.listDetails.isFtp,
      mainListId: this.listDetails.uploadedNewListAppendId
    };
    return this.http.post(
      `${API_URL}ProcessingListSaveAndContinue`,
      reqPayload,
      this.httpOptions
    );
  }

  getUploadStatus(): Observable<any> {
    return this.http.post(
      `${API_URL}GetTargetUploadStatus`,
      { uploadId: this.listDetails.uploadId },
      this.httpOptions
    );
  }

  /**
   * Manage Email Notification
   */
  getNoticationEmail(): Observable<any> {
    return this.http.post(
      `${API_URL}ManageNotificationEmail`,
      { clientId: this.clientId },
      this.httpOptions
    );
  }

  saveNotificationInfo(info: any): Observable<any> {
    const requestPayload = {
      clientId: this.clientId,
      emailAddress: info.emailAddress,
      isRemember: info.isRemember ? 1 : 0
    };
    return this.http.post(
      `${API_URL}SaveClientTargetUploadNotificationInfo`,
      requestPayload,
      this.httpOptions
    );
  }

  // SourceForProgramListCreateTargetUpdateMappingCancel
}
