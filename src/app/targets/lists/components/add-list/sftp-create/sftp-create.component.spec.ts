import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SftpCreateComponent } from './sftp-create.component';

describe('SftpCreateComponent', () => {
  let component: SftpCreateComponent;
  let fixture: ComponentFixture<SftpCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SftpCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
