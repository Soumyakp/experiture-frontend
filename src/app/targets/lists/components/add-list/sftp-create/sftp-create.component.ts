import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddListService, SourceData } from '../add-list.service';

@Component({
  selector: 'app-sftp-create',
  templateUrl: './sftp-create.component.html',
  styleUrls: ['./sftp-create.component.scss']
})
export class SftpCreateComponent implements OnInit {
  sftpListCreateForm: FormGroup = new FormGroup({
    fileName: new FormControl(null, Validators.required),
    fileType: new FormControl(null, Validators.required),
    ftpType: new FormControl(null, Validators.required),
    hostName: new FormControl(null, Validators.required),
    userName: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    port: new FormControl(null, Validators.required)
  });
  fileType = [
    { value: 1, name: 'CSV' },
    { value: 2, name: 'XLSX' }
  ];
  ftpType = [
    { value: 1, name: 'Simple FTP(FTP)' },
    { value: 2, name: 'Secured FTP(SFTP)' },
    { value: 3, name: 'Secured FTP(FTPs)' }
  ];

  @Output() backEvent = new EventEmitter();
  @Output() denyModal = new EventEmitter();
  @Output() ftpListCreated = new EventEmitter();
  sourceData: SourceData;

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {
    this.sourceData = this.addListService.listDetails.sourceData;
  }

  onFtpListCreate(): void {
    this.ftpListCreated.emit({
      value: 'api return!'
    });
  }

  backToCreateList(): void {
    this.backEvent.emit(this.sourceData.prevPageInfo);
  }

  deny(): void {
    this.denyModal.emit(true);
  }
}
