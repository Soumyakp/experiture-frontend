import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AddListService, SourceData } from '../add-list.service';

@Component({
  selector: 'app-skip-dup',
  templateUrl: './skip-dup.component.html',
  styleUrls: ['./skip-dup.component.scss']
})
export class SkipDupComponent implements OnInit {
  task = {
    name: 'All Categories',
    completed: false,
    color: 'primary',
    subtasks: []
  };
  selectedOption = '1';
  selectedSubOption: string;
  loading: boolean;

  @Output() listProcessed = new EventEmitter();
  @Output() denyModal = new EventEmitter();
  sourceData: SourceData;

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {
    this.sourceData = this.addListService.listDetails.sourceData;
    this.getCategoryList();
  }

  private getCategoryList(): void {
    this.loading = true;
    this.addListService.emailCategoryList().subscribe(
      res => {
        this.loading = false;
        this.task.subtasks = res.data.map(item => {
          return {
            name: item.emailCategoryName.trim(),
            emailCategoryId: item.emailCategoryId,
            completed: false,
            color: 'primary'
          };
        });
      },
      error => {
        this.loading = false;
      }
    );
  }

  optionHandler(): void {
    if (this.selectedOption === '1') {
      this.selectedSubOption = undefined;
    }
  }

  saveAndContinue(): void {
    const emailCatIds = [];
    this.task.subtasks.forEach(item => {
      if (item.completed) {
        emailCatIds.push(item.emailCategoryId);
      }
    });
    const reqObj = {
      skip: this.selectedOption === '1',
      merge: false, // not require
      allow: this.selectedOption === '2',
      overwrite: this.selectedSubOption === '3',
      mergeMissing: this.selectedSubOption === '4',
      // campListId: 0,
      // uploadId: 0,
      // isFtp: 'false',
      // mainListId: 0,
      emailCatIdsString: emailCatIds.join(',')
    };
    console.log(this.task);
    console.log(reqObj);
    console.log(this.addListService.listDetails);
    // if (this.sourceData.creationType === 'Manual') {
    //   reqObj.isFtp = 'false';
    // } else if (this.sourceData.creationType === 'SFTP') {
    //   reqObj.isFtp = 'true';
    // }

    // this.listProcessed.emit({
    //   value: 'api return!'
    // });
    this.addListService.saveCategorySetting(reqObj).subscribe(res => {
      if (res.data) {
        this.addListService.listDetails = {
          ...this.addListService.listDetails,
          completed: true
        };
      }
      this.listProcessed.emit({
        value: res.data
      });
    });
  }

  deny(): void {
    this.denyModal.emit(true);
  }

  updateAllComplete(): void {
    this.task.completed =
      this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return (
      this.task.subtasks.filter(t => t.completed).length > 0 &&
      !this.task.completed
    );
  }

  setAll(completed: boolean): void {
    this.task.completed = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => (t.completed = completed));
  }
}
