import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkipDupComponent } from './skip-dup.component';

describe('SkipDupComponent', () => {
  let component: SkipDupComponent;
  let fixture: ComponentFixture<SkipDupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkipDupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkipDupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
