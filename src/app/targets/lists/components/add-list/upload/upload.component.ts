import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { interval } from 'rxjs';
import { AddListService } from '../add-list.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @Output() listUploaded = new EventEmitter();
  @Output() denyModal = new EventEmitter();
  showProgress: boolean;
  fileName: string;
  selectedType: any;
  progressValue: number;
  fileType = [
    { value: 1, name: 'CSV' },
    { value: 2, name: 'XLSX' }
  ];
  showProcessingFile: boolean;

  @ViewChild('uploader') uploader: ElementRef;

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {}

  handleFileInput(file: any): void {
    this.fileName = file.item(0).name;
    this.selectedType = file.item(0).name.endsWith('csv')
      ? { value: 1, name: 'CSV' }
      : { value: 2, name: 'XLSX' };
  }

  removeFile(): void {
    this.uploader.nativeElement.value = '';
    this.fileName = undefined;
  }

  uploadFile(): void {
    this.showProgress = true;
    this.progressValue = 0;
    const source = interval(10);
    const subscribe = source.subscribe(val => {
      if (this.progressValue <= 99) {
        this.progressValue = val;
      }
    });
    if (this.addListService.listDetails.sourceData.creationType === 'Manual') {
      setTimeout(() => {
        this.showProcessingFile = true;
      }, 1000);
      this.addListService
        .targetUpload(this.uploader.nativeElement.files[0])
        .subscribe(
          uploadRes => {
            this.progressValue = 100;
            subscribe.unsubscribe();
            this.showProcessingFile = false;
            this.addListService.listDetails = {
              uploadFileType: this.selectedType.name,
              ...this.addListService.listDetails,
              ...uploadRes.data
            };
            this.listUploaded.emit({ value: uploadRes.data });
            console.log(this.addListService.listDetails);
          },
          error => {
            subscribe.unsubscribe();
            this.showProcessingFile = false;
            this.listUploaded.emit(error);
          }
        );
    } else if (
      this.addListService.listDetails.sourceData.creationType === 'SFTP'
    ) {
      this.progressValue = 100;
      subscribe.unsubscribe();
      this.listUploaded.emit({
        value: 'Api response SFTP'
      });
    }

    // const fileReader = new FileReader();
    // fileReader.readAsDataURL(this.uploader.nativeElement.files[0]);
    // fileReader.onload = e => {
    //   this.manualService
    //     .targetUpload(reqBody, fileReader.result)
    //     .subscribe(res => {
    //       this.progressValue = 100;
    //       console.log('res', res);
    //     });
    // };
  }

  onUploadDeny(): void {
    this.denyModal.emit(true);
  }
}
