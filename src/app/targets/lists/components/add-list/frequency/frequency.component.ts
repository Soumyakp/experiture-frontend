import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.scss']
})
export class FrequencyComponent implements OnInit, AfterViewChecked {
  timeList = [
    {
      id: 1,
      displayName: '12:00 AM'
    },
    {
      id: 2,
      displayName: '12:30 AM'
    },
    {
      id: 3,
      displayName: '01:00 AM'
    },
    {
      id: 3,
      displayName: '01:00 AM'
    },
    {
      id: 3,
      displayName: '01:00 AM'
    },
    {
      id: 3,
      displayName: '01:00 AM'
    },
    {
      id: 3,
      displayName: '01:00 AM'
    }
  ];
  timeTypes = [
    {
      id: 1,
      displayName: 'Minutes'
    },
    {
      id: 2,
      displayName: 'Hours'
    }
  ];

  weekly = {
    name: 'All',
    completed: false,
    color: 'primary',
    subtasks: [
      { name: 'Mon', completed: false, color: 'primary' },
      { name: 'Tue', completed: false, color: 'primary' },
      { name: 'Wed', completed: false, color: 'primary' },
      { name: 'Thu', completed: false, color: 'primary' },
      { name: 'Fri', completed: false, color: 'primary' },
      { name: 'Sat', completed: false, color: 'primary' },
      { name: 'Sun', completed: false, color: 'primary' }
    ]
  };

  frequencyForm: FormGroup = new FormGroup({
    frequency: new FormControl('1', Validators.required),
    startTime: new FormControl(null, Validators.required),
    interval: new FormControl(null),
    timeType: new FormControl(null),
    weekday: new FormArray([]),
    dayDate: new FormControl(null),
    isEnabled: new FormControl(null)
  });

  get toggleFrequency(): string {
    return this.frequencyForm.get('frequency').value;
  }

  get weekDays(): FormArray {
    return this.frequencyForm.get('weekday') as FormArray;
  }

  @Output() frequencySet = new EventEmitter();

  constructor(private cdr: ChangeDetectorRef, private fb: FormBuilder) {}

  ngOnInit(): void {
    // this.frequencyForm.patchValue({
    //   frequency: '1'
    // });
    this.weekly.subtasks.forEach(item => {
      this.weekDays.push(this.fb.control(null));
    });
  }

  frequencyHandler(): void {
    switch (this.toggleFrequency) {
      case '1':
        this.frequencyForm.reset();
        this.resetWeekly();
        this.frequencyForm.patchValue({
          frequency: '1'
        });
        this.frequencyForm
          .get('startTime')
          .setValidators([Validators.required]);
        this.frequencyForm.get('startTime').updateValueAndValidity();
        this.frequencyForm.get('interval').clearValidators();
        this.frequencyForm.get('interval').updateValueAndValidity();
        this.frequencyForm.get('timeType').clearValidators();
        this.frequencyForm.get('timeType').updateValueAndValidity();
        this.frequencyForm.get('weekday').clearValidators();
        this.frequencyForm.get('weekday').updateValueAndValidity();
        this.frequencyForm.get('dayDate').clearValidators();
        this.frequencyForm.get('dayDate').updateValueAndValidity();
        this.cdr.detectChanges();
        break;
      case '2':
        this.frequencyForm.reset();
        this.resetWeekly();
        this.frequencyForm.patchValue({
          frequency: '2'
        });
        this.frequencyForm.get('startTime').clearValidators();
        this.frequencyForm.get('startTime').updateValueAndValidity();
        this.frequencyForm.get('interval').setValidators([Validators.required]);
        this.frequencyForm.get('interval').updateValueAndValidity();
        this.frequencyForm.get('timeType').setValidators([Validators.required]);
        this.frequencyForm.get('timeType').updateValueAndValidity();
        this.frequencyForm.get('weekday').clearValidators();
        this.frequencyForm.get('weekday').updateValueAndValidity();
        this.frequencyForm.get('dayDate').clearValidators();
        this.frequencyForm.get('dayDate').updateValueAndValidity();
        this.frequencyForm.updateValueAndValidity();
        this.cdr.detectChanges();
        break;
      case '3':
        this.frequencyForm.reset();
        this.resetWeekly();
        this.frequencyForm.patchValue({
          frequency: '3'
        });
        this.frequencyForm
          .get('startTime')
          .setValidators([Validators.required]);
        this.frequencyForm.get('startTime').updateValueAndValidity();
        this.frequencyForm.get('interval').clearValidators();
        this.frequencyForm.get('interval').updateValueAndValidity();
        this.frequencyForm.get('timeType').clearValidators();
        this.frequencyForm.get('timeType').updateValueAndValidity();
        this.frequencyForm
          .get('weekday')
          .setValidators([this.formArrayPerControlsValidation()]);
        this.frequencyForm.get('weekday').updateValueAndValidity();
        this.frequencyForm.get('dayDate').clearValidators();
        this.frequencyForm.get('dayDate').updateValueAndValidity();
        this.frequencyForm.updateValueAndValidity();
        this.cdr.detectChanges();
        break;
      case '4':
        this.frequencyForm.reset();
        this.resetWeekly();
        this.frequencyForm.patchValue({
          frequency: '4'
        });
        this.frequencyForm
          .get('startTime')
          .setValidators([Validators.required]);
        this.frequencyForm.get('startTime').updateValueAndValidity();
        this.frequencyForm.get('interval').clearValidators();
        this.frequencyForm.get('interval').updateValueAndValidity();
        this.frequencyForm.get('timeType').clearValidators();
        this.frequencyForm.get('timeType').updateValueAndValidity();
        this.frequencyForm.get('weekday').clearValidators();
        this.frequencyForm.get('weekday').updateValueAndValidity();
        this.frequencyForm.get('dayDate').setValidators([Validators.required]);
        this.frequencyForm.get('dayDate').updateValueAndValidity();
        this.frequencyForm.updateValueAndValidity();
        this.cdr.detectChanges();
        break;
      default:
        break;
    }
    // if (this.toggleFrequency === '1') {

    // } else if ()
  }

  private formArrayPerControlsValidation(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let valid = false;
      formArray.controls.forEach((item: FormControl) => {
        if (item.value) {
          valid = true;
        }
      });
      return valid ? null : { error: 'No day selected!' };
    };
  }

  private resetWeekly(): void {
    this.weekly.completed = false;
    this.weekly.subtasks.forEach(item => {
      item.completed = false;
    });
  }

  submitForm(): void {
    console.log(this.frequencyForm.value);
    console.log(this.weekly.subtasks);
    this.frequencySet.emit({
      value: 'API response!'
    });
  }

  ngAfterViewChecked(): void {}

  updateAllComplete(task: any, index: any): void {
    console.log('index', this.weekDays.controls[index]);
    this.weekly.subtasks.forEach(item => {
      if (item.name === task.name) {
        item.completed = this.weekDays.value[index];
      }
    });
    this.weekly.completed =
      this.weekly.subtasks != null &&
      this.weekly.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.weekly.subtasks == null) {
      return false;
    }
    return (
      this.weekly.subtasks.filter(t => t.completed).length > 0 &&
      !this.weekly.completed
    );
  }

  setAll(completed: boolean): void {
    this.weekly.completed = completed;
    if (this.weekly.subtasks == null) {
      return;
    }
    this.weekly.subtasks.forEach(t => (t.completed = completed));
    this.frequencyForm.patchValue({
      weekday: this.weekly.subtasks.map(item => item.completed)
    });
  }
}
