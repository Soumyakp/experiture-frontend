import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { interval } from 'rxjs';
import { AddListService } from '../add-list.service';

@Component({
  selector: 'app-email-noti',
  templateUrl: './email-noti.component.html',
  styleUrls: ['./email-noti.component.scss']
})
export class EmailNotiComponent implements OnInit {
  appendingDataLoading: boolean;
  msgs = [];
  emailForm = new FormGroup({
    emailAddress: new FormControl(null, [
      Validators.required,
      Validators.email
    ]),
    isRemember: new FormControl(false)
  });
  loading: boolean;
  @Output() denyModal = new EventEmitter();

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {
    this.getUploadStatus();
    this.getEmail();
    setTimeout(() => {
      this.appendingDataLoading = false;
      this.msgs.push(
        {
          severity: 'success',
          summary: '',
          detail: 'Data appended successfully'
        },
        {
          severity: 'success',
          summary: '',
          detail: 'File queued.'
        },
        {
          severity: 'success',
          summary: '',
          detail: 'Finalizing Target Upload.'
        }
      );
    }, 5000);
  }

  private getUploadStatus(): void {
    const source = interval(10);
    const subscribe = source.subscribe(val => {
      console.log(val);
      console.log(source);
    });
    this.addListService.getUploadStatus().subscribe(res => {
      console.log(res);
    });
  }

  private getEmail(): void {
    this.loading = true;
    this.addListService.getNoticationEmail().subscribe(res => {
      this.loading = false;
      this.appendingDataLoading = true;
      console.log(res.data[0]);
      if (res.data[0].isRemember) {
        this.emailForm.patchValue({
          emailAddress: res.data[0].email,
          isRemember: true
        });
      }
    });
  }

  saveFile(): void {
    console.log(this.emailForm.value);
    this.addListService
      .saveNotificationInfo(this.emailForm.value)
      .subscribe(res => {
        console.log(res.data);
      });
  }

  deny(): void {
    this.denyModal.emit(true);
  }
}
