import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailNotiComponent } from './email-noti.component';

describe('EmailNotiComponent', () => {
  let component: EmailNotiComponent;
  let fixture: ComponentFixture<EmailNotiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailNotiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailNotiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
