import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddListService, SourceData } from '../add-list.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  listCreateForm: FormGroup = new FormGroup({
    listName: new FormControl(null, Validators.required),
    listDescription: new FormControl(null),
    destination: new FormControl(null, Validators.required)
  });
  @Input() destinationData: any[];
  @Output() listCreated = new EventEmitter();
  @Output() denyModal = new EventEmitter();
  loading: boolean;
  sourceData: SourceData;

  constructor(private addListService: AddListService) {}

  ngOnInit(): void {
    this.listCreateForm.patchValue({
      destination: this.destinationData.find(item => item.isPrimary)
    });
    this.sourceData = this.addListService.listDetails.sourceData;
    if (this.sourceData.prevPageInfo) {
      this.listCreateForm.patchValue({
        listName: this.sourceData.prevPageInfo.listName,
        listDescription: this.sourceData.prevPageInfo.listDescription,
        destination: this.sourceData.prevPageInfo.destination
      });
    }
  }

  onListCreate(creationType: string): void {
    if (creationType === 'Manual') {
      this.loading = true;
      const userInput = {
        ...this.addListService.listDetails,
        listName: this.listCreateForm.get('listName').value,
        listDescription: this.listCreateForm.get('listDescription').value
          ? this.listCreateForm.get('listDescription').value
          : '',
        relationalObjectName: this.listCreateForm.get('destination').value
          .displayName,
        relationalObjectId: this.listCreateForm.get('destination').value
          .relationalObjectId
      };
      this.addListService.createTarget(userInput).subscribe(
        res => {
          // const listObj = {
          //   listId: res.data.listId,
          //   relationalObjectId: res.data.relationalObjectId,
          //   relationalObjectName: res.data.relationalObjectName
          // };
          this.addListService.listDetails = {
            ...this.addListService.listDetails,
            ...userInput,
            listId: +res.data.listId,
            relationalObjectName: res.data.relationalObjectName
          };
          console.log('aswd', this.addListService.listDetails);
          this.loading = false;
          this.listCreated.emit(res.data);
          this.listCreateForm.reset();
          this.listCreateForm.updateValueAndValidity();
        },
        error => {
          this.loading = false;
        }
      );
    } else if (creationType === 'SFTP') {
      // const frmDetails = {
      //   creationType,
      //   ...this.listCreateForm.value
      // };
      this.addListService.listDetails = {
        ...this.addListService.listDetails,
        ...this.listCreateForm.value
      };
      this.listCreated.emit(this.listCreateForm.value);
    }
  }

  deny(): void {
    this.denyModal.emit(true);
  }
}
