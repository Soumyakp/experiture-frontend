import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface List {
  listName: string;
  listDescription: string;
}

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit, OnChanges {
  @Input() visible: boolean;
  @Input() listName: string;
  @Input() description: string;
  @Output() editItem = new EventEmitter<List>();
  @Output() visibleChange = new EventEmitter();
  listEditForm: FormGroup = new FormGroup({
    listName: new FormControl(null, Validators.required),
    listDescription: new FormControl(null)
  });

  constructor() {}

  ngOnInit(): void {}

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  ngOnChanges(): void {
    this.listEditForm.patchValue({
      listName: this.listName,
      listDescription: this.description ? this.description : ''
    });
  }

  onListEdit(): void {
    this.editItem.emit(this.listEditForm.value);
  }
}
