import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CrmGridComponent } from './crm-grid.component';

describe('CrmGridComponent', () => {
  let component: CrmGridComponent;
  let fixture: ComponentFixture<CrmGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CrmGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrmGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
