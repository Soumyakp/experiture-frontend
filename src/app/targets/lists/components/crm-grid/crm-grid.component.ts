import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import { CRMColumns } from '../../config/list-config';
import { Table } from 'primeng/table';
import { CrmListService } from '../../services/crm-list.service';
import { LazyLoadEvent } from 'primeng/api';
import { TableUtilsService } from '../../services/table-utils.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { CommonService } from 'src/app/shared/service/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crm-grid',
  templateUrl: './crm-grid.component.html',
  styleUrls: ['./crm-grid.component.scss']
})
export class CrmGridComponent implements OnInit {
  loading: boolean;
  targetCountLoading: boolean;
  columns = CRMColumns;
  crmData = [];
  dbColumns = [];
  globalFilterFields = [];
  totalRecord: number;
  targetCount: number;
  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  //
  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  editModal: boolean;
  crmEditModal: boolean;
  recordDetails: any;
  exportModal: boolean;

  constructor(
    private crmService: CrmListService,
    private cdr: ChangeDetectorRef,
    private tableUtils: TableUtilsService,
    private toasterService: ToasterService,
    private commonService: CommonService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.crmData = this.tableUtils.resetDataSource(
        this.crmData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }

    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    // Caching required for virtual scroll
    // if (sortExpression) {
    //   this.table.clearCache();
    // }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.crmService
      .getCrmList(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          // console.log('list', list);
          // if (list.data.totalCount <= 2) {
          //   this.scrollHeight = '100px';
          // } else if (list.data.totalCount > 2 && list.data.totalCount <= 4) {
          //   this.scrollHeight = '200px';
          // } else if (list.data.totalCount > 4 && list.data.totalCount <= 6) {
          //   this.scrollHeight = '300px';
          // } else {
          //   this.scrollHeight = '400px';
          // }
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.crmService.getCrmTargetCount(searchText).subscribe(
              targetCount => {
                this.targetCountLoading = false;
                this.targetCount = targetCount.data.targetCount;
              },
              error => {
                this.targetCountLoading = false;
              }
            );
          }
          if (this.paginator) {
            this.crmData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.crmData = this.tableUtils.updateDataSource(
              this.crmData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.crmData = [...this.crmData];
            this.tRecords = this.crmData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  gotoPage(page: string): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      // console.log(event);
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  showEditDialog(item: any, rowIndex: number): void {
    if (item.listSourceType === 'SalesForce') {
      this.crmEditModal = true;
    } else {
      this.editModal = true;
    }
    this.recordDetails = { rowIndex, ...item };
  }

  onListEdit(list: any): void {
    this.editModal = false;
    const listData = {
      listSource: this.recordDetails.listSourceId,
      listYear: this.recordDetails.listYear,
      listId: this.recordDetails.clientListId,
      ...list
    };
    this.loading = true;
    this.crmService.editList(listData).subscribe(
      res => {
        // TODO: Implement res.status === 'success' check in all needed parts
        // To prevent unnecessary resfesh or reset
        if (res.status === 'success') {
          this.toasterService.showSuccess('List updated successfully!');
          if (this.paginator) {
            this.onRefresh();
          } else {
            // this.table.clearCache();
            this.table.clearState();
            this.table.reset();
            this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
          }
        } else {
          this.loading = false;
        }
      },
      error => {
        this.loading = false;
      }
    );
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.crmService.exportCrm(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.crmService.exportCrm(this.recordDetails).subscribe(res => {
        this.toasterService.showExportSuccess(
          'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
        );
      });
    }
  }

  confirm(): void {}

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=Crm`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSource' },
        fragment: 'crm'
      });
    }
  }
}
