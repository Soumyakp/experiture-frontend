import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RelationSftpGridComponent } from './relation-sftp-grid.component';

describe('RelationSftpGridComponent', () => {
  let component: RelationSftpGridComponent;
  let fixture: ComponentFixture<RelationSftpGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RelationSftpGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelationSftpGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
