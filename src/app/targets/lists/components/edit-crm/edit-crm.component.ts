import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-crm',
  templateUrl: './edit-crm.component.html',
  styleUrls: ['./edit-crm.component.scss']
})
export class EditCrmComponent implements OnInit {
  @Input() visible: boolean;
  @Input() listName: string;
  @Input() description: string;
  @Output() editItem = new EventEmitter();
  @Output() visibleChange = new EventEmitter();
  addSourceForm: FormGroup = new FormGroup({
    username: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    token: new FormControl(null, Validators.required),
    remberme: new FormControl(null)
  });
  saveInfoForm: FormGroup = new FormGroup({
    password: new FormControl(null, Validators.required),
    token: new FormControl(null, Validators.required)
  });
  header = 'Add Source';
  showDefault = true;
  showSalesforce: boolean;
  showResetPass: boolean;

  constructor() {}

  ngOnInit(): void {}

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  next(): void {}

  openSalesforce(): void {
    this.showDefault = false;
    this.showSalesforce = true;
    this.header = 'Edit Login Details';
  }

  backtoHome(): void {
    this.showDefault = true;
    this.showSalesforce = false;
    this.showResetPass = false;
  }

  openResetPass(): void {
    this.showDefault = false;
    this.showResetPass = true;
    this.header = 'Edit Login Details';
  }
}
