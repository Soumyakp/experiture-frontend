import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SftpGridComponent } from './sftp-grid.component';

describe('SftpGridComponent', () => {
  let component: SftpGridComponent;
  let fixture: ComponentFixture<SftpGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SftpGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SftpGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
