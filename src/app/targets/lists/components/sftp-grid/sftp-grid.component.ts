import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import { Table } from 'primeng/table';
import { LazyLoadEvent } from 'primeng/api';
import { SFTPColumns } from '../../config/list-config';
import { SftpListService } from '../../services/sftp-list.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { TableUtilsService } from '../../services/table-utils.service';
import { CommonService } from 'src/app/shared/service/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sftp-grid',
  templateUrl: './sftp-grid.component.html',
  styleUrls: ['./sftp-grid.component.scss']
})
export class SftpGridComponent implements OnInit {
  loading: boolean;
  targetCountLoading: boolean;
  columns = SFTPColumns;
  sftpData = [];
  dbColumns = [];
  globalFilterFields = [];
  totalRecord: number;
  targetCount: number;
  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  //
  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  deleteModal: boolean;
  recordDetails: any;
  exportModal: boolean;

  constructor(
    private sftpService: SftpListService,
    private cdr: ChangeDetectorRef,
    private toasterService: ToasterService,
    private tableUtils: TableUtilsService,
    private commonService: CommonService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.sftpData = this.tableUtils.resetDataSource(
        this.sftpData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }
    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    // Caching required for virtual scroll
    // if (sortExpression) {
    //   this.table.clearCache();
    // }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.sftpService
      .getSftpList(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.sftpService.getSftpTargetCount(searchText).subscribe(
              targetCount => {
                this.targetCountLoading = false;
                this.targetCount = targetCount.data.targetCount;
              },
              error => {
                this.targetCountLoading = false;
              }
            );
          }
          if (this.paginator) {
            this.sftpData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.sftpData = this.tableUtils.updateDataSource(
              this.sftpData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.sftpData = [...this.sftpData];
            this.tRecords = this.sftpData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  gotoPage(page: string): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.sftpService.exportSftp(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.sftpService.exportSftp(this.recordDetails).subscribe(res => {
        this.toasterService.showExportSuccess(
          'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
        );
      });
    }
  }

  addList(): void {}

  showEditDialog(item: any, rowIndex: number): void {}

  confirm(item: any, rowIndex: number): void {
    this.deleteModal = true;
    this.recordDetails = { rowIndex, ...item };
  }

  onDeleteList(isConfirm: boolean): void {
    this.deleteModal = false;
    if (isConfirm) {
      const listData = {
        clientListId: this.recordDetails.clientListId,
        clientId: this.recordDetails.clientId
      };
      this.sftpService.deleteSftp(listData).subscribe(res => {
        if (res.status === 'success') {
          this.toasterService.showSuccess('List removed successfully!');
        }
        if (this.paginator) {
          this.onRefresh();
        } else {
          // this.table.clearCache();
          this.table.clearState();
          this.table.reset();
          this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
        }
      });
    }
  }

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=Ftp`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSource' },
        fragment: 'sftp'
      });
    }
  }
}
