import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonService } from 'src/app/shared/service/common.service';

@Component({
  selector: 'app-add-record',
  templateUrl: './add-record.component.html',
  styleUrls: ['./add-record.component.scss']
})
export class AddRecordComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  @Input() listDetails: any;
  loadingModal: boolean;
  iFrameSrc: string;

  // New var
  header = 'Add Record';
  showDefault = true;
  showAddTarget: boolean;
  showUploadTarget: boolean;

  constructor(private commonService: CommonService) {}

  ngOnInit(): void {
    if (this.commonService.getParamInfo()) {
      this.loadingModal = true;
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/SelectAddRecordType.aspx?lid=${this.listDetails.clientListId}&aid=0`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      this.iFrameSrc = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
      // window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }
  // For iFrame
  iFrameLoadEvent(): void {
    this.loadingModal = false;
  }

  iFrameToggle(event: any): void {
    console.log(event);
  }

  onVisibleChange(event: any): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  addTargetClicked(): void {
    this.showDefault = false;
    this.showAddTarget = true;
  }

  uploadTargetClicked(): void {
    this.showDefault = false;
    this.showUploadTarget = true;
  }
}
