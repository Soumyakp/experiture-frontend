import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  Router,
  NavigationEnd,
  RouterStateSnapshot,
  RouterState
} from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-manage-list',
  templateUrl: './manage-list.component.html',
  styleUrls: ['./manage-list.component.scss']
})
export class ManageListComponent implements OnInit {
  visible: boolean;
  isSelected: boolean;
  showMenu: boolean;
  @ViewChild('relObjSourceRef') relObjSource: ElementRef;
  @ViewChild('menuRef') menuRef: ElementRef;
  @HostListener('document:click', ['$event'])
  
  clickout(event): void {
    if (!this.relObjSource.nativeElement.contains(event.target)) {
      this.visible = false;
    }
    if (
      !this.menuRef.nativeElement.contains(event.target) &&
      !this.relObjSource.nativeElement.contains(event.target)
    ) {
      this.showMenu = false;
    }
  }

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit(): void {
    const state: RouterState = this.router.routerState;
    const snapshot: RouterStateSnapshot = state.snapshot;
    this.isSelected =
      snapshot.url.endsWith('relational-manual') ||
      snapshot.url.endsWith('relational-sftp')
        ? true
        : false;
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isSelected =
          event.url.endsWith('relational-manual') ||
          event.url.endsWith('relational-sftp')
            ? true
            : false;
      }
    });
  }

  toggle(): void {
    this.visible = !this.visible;
  }

  toggleMenu(): void {
    this.showMenu = !this.showMenu;
  }
}
