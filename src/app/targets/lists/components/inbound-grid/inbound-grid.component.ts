import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { InboundColumns } from '../../config/list-config';
import { Table } from 'primeng/table';
import { InboundListService } from '../../services/inbound-list.service';
import { TableUtilsService } from '../../services/table-utils.service';
import { CommonService } from 'src/app/shared/service/common.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inbound-grid',
  templateUrl: './inbound-grid.component.html',
  styleUrls: ['./inbound-grid.component.scss']
})
export class InboundGridComponent implements OnInit {
  loading: boolean;
  targetCountLoading: boolean;
  columns = InboundColumns;
  inboundData = [];
  dbColumns = [];
  globalFilterFields = [];
  totalRecord: number;
  targetCount: number;
  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;

  //
  previousEvent: LazyLoadEvent;
  tRecords: number;
  scrollHeight: string;
  paginator: boolean;
  recordDetails: any;
  exportModal: boolean;

  constructor(
    private inboundService: InboundListService,
    private cdr: ChangeDetectorRef,
    private tableUtils: TableUtilsService,
    private commonService: CommonService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
  }

  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.inboundData = this.tableUtils.resetDataSource(
        this.inboundData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }
    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    // Caching required for virtual scroll
    // if (sortExpression) {
    //   this.table.clearCache();
    // }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getProgramList(
      startIndex,
      event.rows,
      globalFilter,
      sortExpression,
      event
    );
  }

  private getProgramList(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    this.inboundService
      .getInboundList(startIndex, endIndex, searchText, sortExpression)
      .subscribe(
        list => {
          // if (list.data.totalCount <= 2) {
          //   this.scrollHeight = '100px';
          // } else if (list.data.totalCount > 2 && list.data.totalCount <= 4) {
          //   this.scrollHeight = '200px';
          // } else if (list.data.totalCount > 4 && list.data.totalCount <= 6) {
          //   this.scrollHeight = '300px';
          // } else {
          //   this.scrollHeight = '400px';
          // }
          list.data.result.map(item => {
            if (
              item.listName.toLowerCase().trim() === 'inbound list' ||
              item.listName.toLowerCase().trim() === 'default list'
            ) {
              item.listSourceType = 'Inbound';
            }
            return item;
          });
          if (this.totalRecord !== list.data.totalCount) {
            this.targetCountLoading = true;
            this.inboundService.getInboundTargetCount(searchText).subscribe(
              targetCount => {
                this.targetCountLoading = false;
                this.targetCount = targetCount.data.targetCount;
              },
              error => {
                this.targetCountLoading = false;
              }
            );
          }
          if (this.paginator) {
            this.inboundData = list.data.result;
            this.tRecords = list.data.totalCount;
          } else {
            this.inboundData = this.tableUtils.updateDataSource(
              this.inboundData,
              event.first,
              event.rows,
              list.data
            );
            // trigger change detection
            this.inboundData = [...this.inboundData];
            this.tRecords = this.inboundData.length;
            this.cdr.detectChanges();
          }
          this.totalRecord = list.data.totalCount;
          this.dbColumns = list.data.columns;
          this.loading = false;
        },
        error => {
          this.loading = false;
        }
      );
  }

  getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  gotoPage(page: string): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  onExportList(item: any): void {
    this.recordDetails = item;
    if (item.listRecordCount === 0) {
      this.toasterService.showInformation(
        'Please add some records before exporting.',
        'Nothing to export!'
      );
    } else if (item.listRecordCount > 1000) {
      this.exportModal = true;
    } else {
      this.commonService.$progress.next(true);
      this.inboundService.exportInbound(item).subscribe(res => {
        this.commonService.downloadFile(res, item.listName);
        this.commonService.$progress.next(false);
      });
    }
  }

  exporthandler(isSaved: boolean): void {
    if (isSaved) {
      this.inboundService.exportInbound(this.recordDetails).subscribe(res => {
        this.toasterService.showExportSuccess(
          'We have received your export request. You will receive an email shortly with a link to download your file. The email will be sent to the email address used to login to the platform.'
        );
      });
    }
  }

  showAllTargets(item: any): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: `/List/ShowLeads.aspx?list_id=${item.clientListId}&frm=List`
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', item.clientListId], {
        queryParams: { source: 'FromSource' },
        fragment: 'inbound'
      });
    }
  }
}
