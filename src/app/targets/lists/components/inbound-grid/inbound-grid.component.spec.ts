import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InboundGridComponent } from './inbound-grid.component';

describe('InboundGridComponent', () => {
  let component: InboundGridComponent;
  let fixture: ComponentFixture<InboundGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InboundGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboundGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
