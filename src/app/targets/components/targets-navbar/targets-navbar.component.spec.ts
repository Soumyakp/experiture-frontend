import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TargetsNavbarComponent } from './targets-navbar.component';

describe('TargetsNavbarComponent', () => {
  let component: TargetsNavbarComponent;
  let fixture: ComponentFixture<TargetsNavbarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetsNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetsNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
