import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/service/common.service';

@Component({
  selector: 'app-targets-navbar',
  templateUrl: './targets-navbar.component.html',
  styleUrls: ['./targets-navbar.component.scss']
})
export class TargetsNavbarComponent implements OnInit {
  isOpen: boolean;
  isSegOpen: boolean;
  isSubOpen: boolean;
  isDataOpen: boolean;

  // #myVal: boolean;
  // set myToggle(val: boolean) {
  //   this.#myVal = !this.#myVal;
  // }
  // get myToggle(): boolean {
  //   return this.#myVal;
  // }
  constructor(private router: Router, private commonService: CommonService) {}

  ngOnInit(): void {}

  isActive(url: string): boolean {
    return (
      this.router.url.includes(url) ||
      this.router.url.includes('/targets/edit-lead')
    );
  }

  toggleNav(): void {
    this.isOpen = !this.isOpen;
  }

  toggleSeg(): void {
    this.isSegOpen = !this.isSegOpen;
  }

  toggleSub(): void {
    this.isSubOpen = !this.isSubOpen;
  }

  toggleData(): void {
    this.isDataOpen = !this.isDataOpen;
  }

  allTargetsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/ShowLeads.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/all-targets', 'all']);
    }
  }

  scoreHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/Scoring.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/score']);
    }
  }

  subscriptionHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/Unsubscribe.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/targets/subscription']);
    }
  }

  supListHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/SuppressedTargets.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/subscription/suppression']);
    }
  }

  supDomainHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/UnsubscribeDomian.aspx?calltype=1'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/subscription/suppressed']);
    }
  }

  dataModalHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/ManageCustomFields.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/data-modal/custom-field']);
    }
  }

  relTableDataHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/CustomObjectData.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['targets/data-modal/custom-object']);
    }
  }
}
