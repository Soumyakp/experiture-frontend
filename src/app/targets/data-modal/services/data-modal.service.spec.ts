import { TestBed } from '@angular/core/testing';

import { DataModalService } from './data-modal.service';

describe('DataModalService', () => {
  let service: DataModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
