import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export interface CustomDataList {  fieldname; fieldtype; relationalfield; options; modifieddate;status}

@Injectable({
  providedIn: 'root'
})

export class DataModalService {

  constructor(private http:HttpClient) { }

  getCustomFieldList()
  {
    return this.http.get<any>('../../../../assets/customdata.json')
    .toPromise()
    .then(res =><CustomDataList[]>res.data )
    .then(data => {return data;});
  }

}
