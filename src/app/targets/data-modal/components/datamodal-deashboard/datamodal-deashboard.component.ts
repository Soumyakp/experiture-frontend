import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import {LefttablelistComponent} from '../lefttablelist/lefttablelist.component';

@Component({
  selector: 'app-datamodal-deashboard',
  templateUrl: './datamodal-deashboard.component.html',
  styleUrls: ['./datamodal-deashboard.component.scss']
})
export class DatamodalDeashboardComponent implements OnInit {
  showMenu: boolean;
  @ViewChild('menuRef') menuRef: ElementRef;
  @HostListener('document:click', ['$event'])
  clickout(event): void {
    if (!this.menuRef.nativeElement.contains(event.target)) {
      this.showMenu = false;
    }
  }

  constructor(private router: Router) {}

  ngOnInit(): void {}

  toggleMenu(): void {
    this.showMenu = !this.showMenu;
  }

  getTitle(): string {
    if (this.router.url.includes('custom-field')) {
      return 'Manage Custom Fields';
    } else if (this.router.url.includes('custom-object')) {
      return 'Manage Custom Fields';
    }
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (this.router.url.includes('custom-field')) {
          return 'Manage Custom Fields';
        } else if (this.router.url.includes('custom-object')) {
          return 'Manage Custom Fields';
        }
      });
  }
}
