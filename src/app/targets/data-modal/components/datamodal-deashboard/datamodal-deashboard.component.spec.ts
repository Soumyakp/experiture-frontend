import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatamodalDeashboardComponent } from './datamodal-deashboard.component';

describe('DatamodalDeashboardComponent', () => {
  let component: DatamodalDeashboardComponent;
  let fixture: ComponentFixture<DatamodalDeashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatamodalDeashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatamodalDeashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
