import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCustomFieldComponent } from './manage-custom-field.component';

describe('ManageCustomFieldComponent', () => {
  let component: ManageCustomFieldComponent;
  let fixture: ComponentFixture<ManageCustomFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCustomFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCustomFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
