import { Component, OnInit } from '@angular/core';
import {DataModalService,CustomDataList } from '../../services/data-modal.service'

@Component({
  selector: 'app-manage-custom-field',
  templateUrl: './manage-custom-field.component.html',
  styleUrls: ['./manage-custom-field.component.scss']
})
export class ManageCustomFieldComponent implements OnInit {

    customdatalists: CustomDataList[];
    selectedlist: CustomDataList[];
    statuses: any[];

    loading: boolean = true;

    activityValues: number[] = [0, 100];

    constructor(private customDataService: DataModalService) { }

    ngOnInit() {
        this.customDataService.getCustomFieldList().then(customdatalists => {
            this.customdatalists = customdatalists;
            this.loading = false;            
        });

        

        this.statuses = [
            {label: 'Unqualified', value: 'unqualified'},
            {label: 'Qualified', value: 'qualified'},
            {label: 'New', value: 'new'},
            {label: 'Negotiation', value: 'negotiation'},
            {label: 'Renewal', value: 'renewal'},
            {label: 'Proposal', value: 'proposal'}
        ]
    }

}
