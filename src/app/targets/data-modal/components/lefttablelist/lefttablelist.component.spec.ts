import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LefttablelistComponent } from './lefttablelist.component';

describe('LefttablelistComponent', () => {
  let component: LefttablelistComponent;
  let fixture: ComponentFixture<LefttablelistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LefttablelistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LefttablelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
