import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomObjectDataComponent } from './custom-object-data.component';

describe('CustomObjectDataComponent', () => {
  let component: CustomObjectDataComponent;
  let fixture: ComponentFixture<CustomObjectDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomObjectDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomObjectDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
