import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TargetsNavbarComponent } from './components/targets-navbar/targets-navbar.component';
import { ManageListComponent } from './lists/components/manage-list/manage-list.component';
import { TargetsRoutingModule } from './targets-routing.module';
import { ManualGridComponent } from './lists/components/manual-grid/manual-grid.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EditListComponent } from './lists/components/edit-list/edit-list.component';
import { DeleteListComponent } from './lists/components/delete-list/delete-list.component';
import { AddRecordComponent } from './lists/components/add-record/add-record.component';
import { AddListComponent } from './lists/components/add-list/add-list.component';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InboundGridComponent } from './lists/components/inbound-grid/inbound-grid.component';
import { SftpGridComponent } from './lists/components/sftp-grid/sftp-grid.component';
import { TooltipModule } from 'primeng/tooltip';
import { AddSpacePipe } from '../shared/pipe/add-space.pipe';
import { CrmGridComponent } from './lists/components/crm-grid/crm-grid.component';
import { RelationManualGridComponent } from './lists/components/relation-manual-grid/relation-manual-grid.component';
import { RelationSftpGridComponent } from './lists/components/relation-sftp-grid/relation-sftp-grid.component';
import { EditCrmComponent } from './lists/components/edit-crm/edit-crm.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ExportNotificationComponent } from './lists/components/export-notification/export-notification.component';
import { AllTargetsGridComponent } from './all-targets/components/all-targets-grid/all-targets-grid.component';
import { ToastModule } from 'primeng/toast';
import { FieldsModalComponent } from './all-targets/components/fields-modal/fields-modal.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { EditLeadComponent } from './all-targets/components/edit-lead/edit-lead.component';
import { MenuModule } from 'primeng/menu';
import { AccordionModule } from 'primeng/accordion';
import { CreateSegmentComponent } from './segments/components/create-segment/create-segment.component';
import { AddProgramDialogComponent } from './segments/components/add-program-dialog/add-program-dialog.component';
import { SegmentGridComponent } from './segments/components/segment-grid/segment-grid.component';
import { ConditionalSegmentGridComponent } from './segments/components/conditional-segment-grid/conditional-segment-grid.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountListDialogComponent } from '../header/components/account-list-dialog/account-list-dialog.component';

import { SafePipe } from '../shared/pipe/safe.pipe';
import { DeleteDialogComponent } from '../shared/component/delete-dialog/delete-dialog.component';
import { FrequencyComponent } from './lists/components/add-list/frequency/frequency.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { CreateComponent } from './lists/components/add-list/create/create.component';
import { UploadComponent } from './lists/components/add-list/upload/upload.component';
import { MapFieldComponent } from './lists/components/add-list/map-field/map-field.component';
import { SkipDupComponent } from './lists/components/add-list/skip-dup/skip-dup.component';
import { EmailNotiComponent } from './lists/components/add-list/email-noti/email-noti.component';
import { SftpCreateComponent } from './lists/components/add-list/sftp-create/sftp-create.component';
import { OptedComponent } from './subscriptions/components/opted/opted.component';
import { SubDashboardComponent } from './subscriptions/components/sub-dashboard/sub-dashboard.component';
import { SegDashboardComponent } from './segments/components/seg-dashboard/seg-dashboard.component';
import { SuppressionComponent } from './subscriptions/components/suppression/suppression.component';
import { SuppressedComponent } from './subscriptions/components/suppressed/suppressed.component';
import { CustomObjectDataComponent } from './data-modal/components/custom-object-data/custom-object-data.component';
import { ManageCustomFieldComponent } from './data-modal/components/manage-custom-field/manage-custom-field.component';
import { SharedModule } from '../shared/module/shared.module';
import { DatamodalDeashboardComponent } from './data-modal/components/datamodal-deashboard/datamodal-deashboard.component';
import { LefttablelistComponent } from './data-modal/components/lefttablelist/lefttablelist.component';
import { ScoreComponent } from './score/components/score/score.component';
@NgModule({
  declarations: [
    TargetsNavbarComponent,
    ManageListComponent,
    ManualGridComponent,
    EditListComponent,
    DeleteListComponent,
    AddRecordComponent,
    AddListComponent,
    InboundGridComponent,
    SftpGridComponent,
    AddSpacePipe,
    CrmGridComponent,
    RelationManualGridComponent,
    RelationSftpGridComponent,
    EditCrmComponent,
    ExportNotificationComponent,
    AllTargetsGridComponent,
    FieldsModalComponent,
    EditLeadComponent,
    SegDashboardComponent,
    CreateSegmentComponent,
    AddProgramDialogComponent,
    SegmentGridComponent,
    ConditionalSegmentGridComponent,
    AccountListDialogComponent,
    DeleteDialogComponent,
    FrequencyComponent,
    CreateComponent,
    UploadComponent,
    MapFieldComponent,
    SkipDupComponent,
    EmailNotiComponent,
    SftpCreateComponent,
    SubDashboardComponent,
    OptedComponent,
    SuppressionComponent,
    SuppressedComponent,
    CustomObjectDataComponent,
    ManageCustomFieldComponent,
    DatamodalDeashboardComponent,
    LefttablelistComponent,
    ScoreComponent
  ],
  imports: [
    CommonModule,
    TargetsRoutingModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    DropdownModule,
    ProgressBarModule,
    RadioButtonModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    TooltipModule,
    InputTextareaModule,
    ToastModule,
    DragDropModule,
    MenuModule,
    AccordionModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule,
    MatCheckboxModule,
    InputNumberModule,
    SharedModule
  ]
})
export class TargetsModule {}
