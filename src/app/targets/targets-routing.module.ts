import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../auth/auth.guard';
import { TargetsNavbarComponent } from './components/targets-navbar/targets-navbar.component';
import { ManageListComponent } from './lists/components/manage-list/manage-list.component';
import { ManualGridComponent } from './lists/components/manual-grid/manual-grid.component';
import { InboundGridComponent } from './lists/components/inbound-grid/inbound-grid.component';
import { SftpGridComponent } from './lists/components/sftp-grid/sftp-grid.component';
import { CrmGridComponent } from './lists/components/crm-grid/crm-grid.component';
import { RelationManualGridComponent } from './lists/components/relation-manual-grid/relation-manual-grid.component';
import { RelationSftpGridComponent } from './lists/components/relation-sftp-grid/relation-sftp-grid.component';
import { AllTargetsGridComponent } from './all-targets/components/all-targets-grid/all-targets-grid.component';
import { EditLeadComponent } from './all-targets/components/edit-lead/edit-lead.component';
import { CreateSegmentComponent } from './segments/components/create-segment/create-segment.component';
import { SegmentGridComponent } from './segments/components/segment-grid/segment-grid.component';
import { ConditionalSegmentGridComponent } from './segments/components/conditional-segment-grid/conditional-segment-grid.component';
import { SubDashboardComponent } from './subscriptions/components/sub-dashboard/sub-dashboard.component';
import { OptedComponent } from './subscriptions/components/opted/opted.component';
import { SegDashboardComponent } from './segments/components/seg-dashboard/seg-dashboard.component';
import { SuppressionComponent } from './subscriptions/components/suppression/suppression.component';
import { SuppressedComponent } from './subscriptions/components/suppressed/suppressed.component';
import { CustomObjectDataComponent } from './data-modal/components/custom-object-data/custom-object-data.component';
import { ManageCustomFieldComponent } from './data-modal/components/manage-custom-field/manage-custom-field.component';
import { DatamodalDeashboardComponent } from './data-modal/components/datamodal-deashboard/datamodal-deashboard.component';
import { ScoreComponent } from './score/components/score/score.component';

export const routes: Routes = [
  {
    path: '',
    component: TargetsNavbarComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'lists',
        pathMatch: 'full'
      },
      {
        path: 'lists',
        component: ManageListComponent,
        children: [
          {
            path: '',
            redirectTo: 'manual',
            pathMatch: 'full'
          },
          {
            path: 'manual',
            component: ManualGridComponent,
            data: { title: 'Data Sources & Lists | Manual' }
          },
          {
            path: 'inbound',
            component: InboundGridComponent,
            data: { title: 'Data Sources & Lists | Inbound' }
          },
          {
            path: 'sftp',
            component: SftpGridComponent,
            data: { title: 'Data Sources & Lists | SFTP' }
          },
          {
            path: 'crm',
            component: CrmGridComponent,
            data: { title: 'Data Sources & Lists | CRM' }
          },
          {
            path: 'relational-manual',
            component: RelationManualGridComponent,
            data: { title: 'Data Sources & Lists | Ralational Manual Object' }
          },
          {
            path: 'relational-sftp',
            component: RelationSftpGridComponent,
            data: { title: 'Data Sources & Lists | Ralational SFTP Object' }
          }
        ]
      },
      {
        path: 'all-targets/:id',
        component: AllTargetsGridComponent,
        data: { title: 'Experiture | All Targets' }
      },
      {
        path: 'edit-lead/:id',
        component: EditLeadComponent,
        data: { title: 'Experiture | Edit Lead' }
      },
      {
        path: 'segment',
        component: SegDashboardComponent,
        children: [
          {
            path: '',
            redirectTo: 'view',
            pathMatch: 'full'
          },
          {
            path: 'view',
            component: SegmentGridComponent,
            data: { title: 'Experiture | View Segments' }
          },
          {
            path: 'conditional',
            component: ConditionalSegmentGridComponent,
            data: { title: 'Experiture | Conditional Segments' }
          }
          // {
          //   path: 'create',
          //   component: CreateSegmentComponent,
          //   data: { title: 'Experiture | Create Segment' }
          // },
          // {
          //   path: 'edit/:id',
          //   component: CreateSegmentComponent,
          //   data: { title: 'Experiture | Edit Segment' }
          // }
        ]
      },
      {
        path: 'segment/create',
        component: CreateSegmentComponent,
        data: { title: 'Experiture | Create Segment' }
      },
      {
        path: 'segment/edit/:id',
        component: CreateSegmentComponent,
        data: { title: 'Experiture | Edit Segment' }
      },
      {
        path: 'score',
        component: ScoreComponent
      },
      {
        path: 'subscription',
        component: SubDashboardComponent,
        children: [
          {
            path: '',
            redirectTo: 'optedout',
            pathMatch: 'full'
          },
          {
            path: 'optedout',
            component: OptedComponent,
            data: { title: 'Experiture | Opted Out Targets' }
          },
          {
            path: 'suppression',
            component: SuppressionComponent,
            data: { title: 'Experiture | Suppression List' }
          },
          {
            path: 'suppressed',
            component: SuppressedComponent,
            data: { title: 'Experiture | Suppressed Domains' }
          }
        ]
      },
      {
        path: 'data-modal',
        component: DatamodalDeashboardComponent,
        children: [
          {
            path: '',
            redirectTo: 'custom-field',
            pathMatch: 'full'
          },
          {
            path: 'custom-field',
            component: ManageCustomFieldComponent,
            data: { title: 'Experiture | Manage Custom Fields' }
          },
          {
            path: 'custom-object',
            component: CustomObjectDataComponent,
            data: { title: 'Experiture | Manage Custom Fields' }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetsRoutingModule {}
