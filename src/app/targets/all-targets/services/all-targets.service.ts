import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

const API_URL = `${environment.API_URL}/api/AllTarget/`;

@Injectable({
  providedIn: 'root'
})
export class AllTargetsService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private clientId = +sessionStorage.getItem('CLIENT_ID');
  constructor(private http: HttpClient) {}

  getLeadsById(
    clientListId: number,
    showLeadSource: string,
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'Modified_Date desc'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      clientId: this.clientId,
      userId: +sessionStorage.getItem('USER_ID'),
      roleId: 0,
      startIndex,
      endIndex,
      searchField: 'All_Fields',
      searchText,
      sortExpression,
      clientListId,
      showLeadSource
    };
    return this.http.post(`${API_URL}GetSourceLeads`, reqObj, this.httpOptions);
  }

  getLeadsCountById(clientListId: number, searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      searchText,
      clientListId,
      userId: +sessionStorage.getItem('USER_ID'),
      roleId: 0,
      programId: 0,
      from: '',
      startIndex: 0,
      endIndex: 10,
      searchField: '',
      sortExpression: ''
    };
    return this.http.post(
      `${API_URL}GetSourceLeadsCount`,
      reqObj,
      this.httpOptions
    );
  }

  getAllTargets(
    startindex = 0,
    endIndex = 10,
    searchText = '',
    sortExpression = 'First_Name'
  ): Observable<any> {
    const startIndex = startindex === 0 ? 0 : startindex + 1;
    const reqObj = {
      clientId: this.clientId,
      clientListId: 3423,
      maxRow: endIndex,
      minRow: startIndex,
      orderBy: sortExpression,
      filter: searchText
    };
    return this.http.post(
      `${API_URL}GetSourceLeadsTarget`,
      reqObj,
      this.httpOptions
    );
  }

  getAllTargetsCount(searchText = ''): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      filter: searchText
    };
    return this.http.post(
      `${API_URL}GetAllTargetCount`,
      reqObj,
      this.httpOptions
    );
  }

  deleteTargets(ids: string): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      ids
    };
    return this.http.post(
      `${API_URL}DeleteSelectedTargets`,
      reqObj,
      this.httpOptions
    );
  }

  getAvailableFields(listId: number): Observable<any> {
    const reqObj = {
      listId,
      clientId: this.clientId
    };
    return this.http.post(
      `${API_URL}GetCampaignListDisplayFields`,
      reqObj,
      this.httpOptions
    );
  }

  updateDisplayFields(name: string): Observable<any> {
    const reqObj = {
      clientId: this.clientId,
      fields: name
    };
    return this.http.post(
      `${API_URL}UpdateProgramTargetDisplayFields`,
      reqObj,
      this.httpOptions
    );
  }
}
