import { TestBed } from '@angular/core/testing';

import { AllTargetsService } from './all-targets.service';

describe('AllTargetsService', () => {
  let service: AllTargetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllTargetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
