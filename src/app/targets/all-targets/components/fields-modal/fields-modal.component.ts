import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { AllTargetsService } from '../../services/all-targets.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fields-modal',
  templateUrl: './fields-modal.component.html',
  styleUrls: ['./fields-modal.component.scss']
})
export class FieldsModalComponent implements OnInit {
  @Input() visible: boolean;
  @Input() selectedFields: boolean;
  @Output() visibleChange = new EventEmitter();
  items = [
    'Lead owner',
    'Last activity date',
    'Last activity',
    'Most recent program',
    'Profile URL',
    'Record added date',
    'Title',
    'Telephone main',
    'Telephone mobile',
    'Fax',
    'Address line 1',
    'Address line 2',
    'Country',
    'Website URL'
  ];
  basket = ['First name', 'Last name', 'Email address'];

  constructor(
    private allTargetsServ: AllTargetsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // const listId = +this.route.snapshot.paramMap.get('id');
    // this.allTargetsServ.getAvailableFields(listId).subscribe(res => {
    //   console.log(res);
    // });
  }

  onVisibleChange(event: boolean): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  drop(event: CdkDragDrop<string[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  updateFields(): void {
    // this.allTargetsServ
    //   .updateDisplayFields('First_Name,Last_Name')
    //   .subscribe(res => {
    //     console.log(res);
    //   });
  }
}
