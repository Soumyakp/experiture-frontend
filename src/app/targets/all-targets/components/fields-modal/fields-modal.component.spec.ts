import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FieldsModalComponent } from './fields-modal.component';

describe('FieldsModalComponent', () => {
  let component: FieldsModalComponent;
  let fixture: ComponentFixture<FieldsModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
