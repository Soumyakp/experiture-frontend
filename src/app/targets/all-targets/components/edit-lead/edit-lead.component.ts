import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-lead',
  templateUrl: './edit-lead.component.html',
  styleUrls: ['./edit-lead.component.scss']
})
export class EditLeadComponent implements OnInit {
  paramId: string;
  queryParam: string;
  leadOwners: [];
  selectedLead: any;
  // detailItems = [
  //   {
  //     label: 'Options',
  //     items: [
  //       {
  //         label: 'Update',
  //         icon: 'pi pi-refresh',
  //         command: () => {
  //           // this.update();
  //         }
  //       },
  //       {
  //         label: 'Delete',
  //         icon: 'pi pi-times',
  //         command: () => {
  //           // this.delete();
  //         }
  //       }
  //     ]
  //   },
  //   {
  //     label: 'Navigate',
  //     items: [
  //       {
  //         label: 'Angular Website',
  //         icon: 'pi pi-external-link',
  //         url: 'http://angular.io'
  //       },
  //       {
  //         label: 'Router',
  //         icon: 'pi pi-upload',
  //         routerLink: '/fileupload'
  //       }
  //     ]
  //   }
  // ];

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.paramId = this.route.snapshot.paramMap.get('id');
    this.queryParam = this.route.snapshot.queryParamMap.get('source');
  }

  backToAllTargets(): void {
    this.router.navigate(['/targets/all-targets', this.paramId], {
      queryParams: { source: this.queryParam }
    });
  }

  save(): void {}
}
