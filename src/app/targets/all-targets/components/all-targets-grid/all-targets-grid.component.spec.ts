import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AllTargetsGridComponent } from './all-targets-grid.component';

describe('AllTargetsGridComponent', () => {
  let component: AllTargetsGridComponent;
  let fixture: ComponentFixture<AllTargetsGridComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AllTargetsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllTargetsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
