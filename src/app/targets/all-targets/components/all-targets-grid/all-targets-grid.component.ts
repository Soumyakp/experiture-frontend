import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { Table } from 'primeng/table';
import { MessageService } from 'primeng/api';

import { TableUtilsService } from '../../../lists/services/table-utils.service';
import { AllTargetsColumns } from '../../config/all-targets-config';
import { AllTargetsService } from '../../services/all-targets.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';

@Component({
  selector: 'app-all-targets-grid',
  templateUrl: './all-targets-grid.component.html',
  styleUrls: ['./all-targets-grid.component.scss'],
  providers: [MessageService]
})
export class AllTargetsGridComponent implements OnInit {
  loading: boolean;
  paramId: string;
  queryParam: string;
  allTargetData = [];
  dbColumns = [];
  columns = AllTargetsColumns;
  paginator: boolean;
  tRecords: number;
  targetCountLoading: boolean;
  targetCount: number;
  scrollHeight: string;
  previousEvent: LazyLoadEvent;
  @ViewChild('table') table: Table;
  @ViewChild('searchText') searchText: ElementRef;
  @ViewChild('pageNo') pageNo: ElementRef;
  totalRecord: number;
  selectedTarget = [];
  addTargetModal: boolean;

  respondedType = [
    {
      label: 'All',
      value: 'ALL'
    },
    {
      label: 'Responded to all programs',
      value: 'AP'
    },
    {
      label: 'Responded to inbound programs',
      value: 'IP'
    },
    {
      label: 'Responded to outbound programs',
      value: 'OP'
    }
  ];
  selectedType = {
    label: 'All',
    value: 'ALL'
  };

  viewList = [
    {
      label: 'Select View...',
      clientListId: null,
      source: 'all'
    },
    {
      label: 'Dummy for 266202 id',
      clientListId: 266202,
      source: 'FromSource'
    }
  ];

  selectedView = {
    label: 'Select View...',
    clientListId: null,
    source: 'all'
  };
  fieldsModal: boolean;
  deleteModal: boolean;
  fragment: string;
  availablefields: any;
  selectedField: any;
  columnHeaders: any;

  constructor(
    private tableUtils: TableUtilsService,
    private allTargetsService: AllTargetsService,
    private route: ActivatedRoute,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private messageService: MessageService,
    private toasterService: ToasterService
  ) {}

  ngOnInit(): void {
    this.refetchRoute();
    this.loading = true;
    this.targetCountLoading = true;
    this.scrollHeight = this.tableUtils.scrollHeight;
    this.paginator = this.tableUtils.paginator;
    this.paramId = this.route.snapshot.paramMap.get('id');
    this.queryParam = this.route.snapshot.queryParamMap.get('source');
    this.fragment = this.route.snapshot.fragment;
  }

  private getLeads(
    startIndex?: number,
    endIndex?: number,
    searchText?: string,
    sortExpression?: string,
    event?: LazyLoadEvent
  ): void {
    if (this.paramId === 'all') {
      // Open all Tergets
      // this.loading = false;
      // this.targetCountLoading = false;
      // this.cdr.detectChanges();
      // this.allTargetData = [];
      // this.toasterService.showInformation('API is not ready yet!');

      // this.allTargetsService.getAllTargetsCount(searchText).subscribe(res => {
      //   console.log(res);
      //   this.loading = false;
      // });
      this.allTargetsService
        .getLeadsById(
          0,
          'FromSource',
          startIndex,
          endIndex,
          searchText,
          sortExpression
        )
        .subscribe(
          target => {
            console.log(target);
            this.loading = false;
            this.targetCountLoading = false;
            if (this.totalRecord !== target.data.totalCount) {
              this.targetCountLoading = true;
              this.allTargetsService.getLeadsCountById(0, searchText).subscribe(
                targetCount => {
                  this.targetCountLoading = false;
                  this.targetCount = targetCount.data.targetCount;
                },
                error => {
                  this.targetCountLoading = false;
                }
              );
            }
            if (this.paginator) {
              this.allTargetData = target.data.result;
              this.tRecords = target.data.totalCount;
            } else {
              this.allTargetData = this.tableUtils.updateDataSource(
                this.allTargetData,
                event.first,
                event.rows,
                target.data
              );
              // trigger change detection
              this.allTargetData = [...this.allTargetData];
              this.tRecords = this.allTargetData.length;
              this.cdr.detectChanges();
            }
            this.totalRecord = target.data.totalCount;
            this.dbColumns = target.data.columns;
            this.loading = false;
          },
          error => {
            this.loading = false;
            this.targetCountLoading = false;
          }
        );
    } else {
      this.allTargetsService
        .getLeadsById(
          +this.paramId,
          this.queryParam,
          startIndex,
          endIndex,
          searchText,
          sortExpression
        )
        .subscribe(
          target => {
            if (this.totalRecord !== target.data.totalCount) {
              this.targetCountLoading = true;
              this.allTargetsService
                .getLeadsCountById(+this.paramId, searchText)
                .subscribe(
                  targetCount => {
                    this.targetCountLoading = false;
                    this.targetCount = targetCount.data.targetCount;
                  },
                  error => {
                    this.targetCountLoading = false;
                  }
                );
            }
            if (this.paginator) {
              this.allTargetData = target.data.result;
              this.tRecords = target.data.totalCount;
            } else {
              this.allTargetData = this.tableUtils.updateDataSource(
                this.allTargetData,
                event.first,
                event.rows,
                target.data
              );
              // trigger change detection
              this.allTargetData = [...this.allTargetData];
              this.tRecords = this.allTargetData.length;
              this.cdr.detectChanges();
            }
            this.totalRecord = target.data.totalCount;
            this.dbColumns = target.data.columns;
            // this.columnHeaders = target.data.columns.map(item => {
            // console.log(item.dbColumnName);
            // return {
            //   label: item.dbColumnName.replace(/_/g, ' '),
            //   value: item.columnName
            // };
            //   return {
            //     label: item.columnName
            //   };
            // });
            this.loading = false;
          },
          error => {
            this.loading = false;
            this.targetCountLoading = false;
          }
        );
    }
  }

  nextPage(event: LazyLoadEvent): void {
    if (this.paginator) {
      this.loading = true;
    } else {
      this.allTargetData = this.tableUtils.resetDataSource(
        this.allTargetData,
        event,
        this.previousEvent,
        this.table
      );
      this.cdr.detectChanges();
      this.previousEvent = JSON.parse(JSON.stringify(event));
    }

    const startIndex = event.first;
    let sortExpression = event.sortField
      ? this.getDBColumnName(event.sortField)
      : undefined;
    if (event.sortOrder === -1) {
      sortExpression = `${sortExpression} desc`;
    }
    const globalFilter = event.globalFilter ? event.globalFilter : '';
    this.getLeads(startIndex, event.rows, globalFilter, sortExpression, event);
  }

  private getDBColumnName(str: string): string {
    return this.dbColumns.find(item => item.columnName === str).dbColumnName;
  }

  resetPage(): void {
    this.loading = true;
    // Caching required for virtual scroll
    if (!this.paginator) {
      // this.table.clearCache();
      this.table.clearState();
      this.table.resetScrollTop();
    }
    this.table.reset();
    this.searchText.nativeElement.value = '';
    this.pageNo.nativeElement.value = '';
  }

  onRefresh(): void {
    const event = {
      first: this.table.first,
      rows: this.table.rows
    };
    this.table.onPageChange(event);
  }

  gotoPage(page: string): void {
    const isNum = /^\d+$/.test(page);
    if (isNum) {
      const rows = this.table.rows;
      const event = {
        first: +page * rows - rows,
        rows
      };
      this.table.onPageChange(event);
      this.pageNo.nativeElement.value = '';
    }
  }

  navigateBack(): void {
    this.router.navigate([`/targets/lists/${this.fragment}`]);
  }

  viewChangeHandler(): void {
    if (this.selectedView.clientListId) {
      this.router.navigate(
        ['/targets/all-targets', this.selectedView.clientListId],
        {
          queryParams: { source: this.selectedView.source }
        }
      );
    }
  }

  deleteTargets(): void {
    if (this.selectedTarget.length > 0) {
      this.deleteModal = true;
    } else {
      this.toasterService.showInformation(
        'Please select target(s) to delete !'
      );
    }
  }

  deletehandler(isDeleted: boolean): void {
    if (isDeleted) {
      this.deleteModal = false;
      const ids = this.selectedTarget
        .map(item => {
          return item.id;
        })
        .join();
      this.loading = true;
      this.allTargetsService.deleteTargets(ids).subscribe(
        res => {
          if (res.status === 'success') {
            this.toasterService.showSuccess('Target(s) removed successfully!');
            if (this.paginator) {
              this.onRefresh();
            } else {
              // this.table.clearCache();
              this.table.clearState();
              this.table.reset();
              // this.table.scrollToVirtualIndex(this.recordDetails.rowIndex);
            }
          } else {
            this.loading = false;
          }
        },
        error => {
          this.loading = false;
        }
      );
    }
  }

  editLead(item: any, rowIndex: number): void {
    this.router.navigate(['/targets/edit-lead', this.paramId], {
      queryParams: { source: this.queryParam }
    });
  }

  private refetchRoute(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = (
      future: ActivatedRouteSnapshot,
      curr: ActivatedRouteSnapshot
    ) => {
      if (
        curr.url.toString().startsWith('all-targets') &&
        future.url.toString().startsWith('all-targets') &&
        curr.url.toString() !== future.url.toString()
      ) {
        return false;
      }
      return future.routeConfig === curr.routeConfig;
    };
  }

  createNewView(): void {}
}
