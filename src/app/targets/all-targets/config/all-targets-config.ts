export const AllTargetsColumns = [
  { field: 'editTarget', header: '', sortable: false },
  { field: 'firstName', header: 'First Name', sortable: true },
  { field: 'lastName', header: 'Last Name', sortable: true },
  { field: 'stateProvince', header: 'State/Province', sortable: true },
  { field: 'company', header: 'Company/Account', sortable: true },
  { field: 'isOfferRedeemed', header: 'Is Offer Redeemed', sortable: true },
  { field: 'tenure', header: 'Tenure', sortable: true },
  { field: 'emailAddress', header: 'Email Address', sortable: true },
  { field: 'city', header: 'City', sortable: true },
  { field: 'zipCode', header: 'Zip/Postal Code', sortable: true },
  { field: 'age', header: 'Age', sortable: true },
  { field: 'lastModifiedDate', header: 'Last Modified Date', sortable: true },
  { field: 'listType', header: 'List Type', sortable: true }
];
