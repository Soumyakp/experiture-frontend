import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/service/common.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  @Output() hideSidebar = new EventEmitter();
  @Input() isRootUser: boolean;

  constructor(private commonService: CommonService, private router: Router) {}

  ngOnInit(): void {}

  onVisibleChange(event: any): void {
    this.visibleChange.emit(event);
    if (event) {
      this.hideSidebar.emit(false);
    }
  }

  displaySidebarHide(): void {
    this.visibleChange.emit(false);
    this.hideSidebar.emit(false);
  }

  dashboardHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/campaigns/AccountDashboard.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/dashboard']);
    }
  }

  dataSourcelistHandler(): void {
    this.displaySidebarHide();
    this.router.navigate(['/targets/lists']);
  }

  allTargetsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/ShowLeads.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/targets/all-targets', 'all']);
    }
  }

  segmentsHandler(): void {
    this.displaySidebarHide();
    this.router.navigate(['/targets/segment']);
  }

  scoreHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/Scoring.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/targets/score']);
    }
  }

  subscriptionHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/Unsubscribe.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/targets/subscription']);
    }
  }

  dataModalHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/ManageCustomFields.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/targets/data-modal']);
    }
  }

  campaignHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Campaigns/ProgramList.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  landingHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Campaigns/LeadPageDashboard.aspx?maintab=true'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  formsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Campaigns/FormsDashboard.aspx?maintab=true&setActive=y'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  leadsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/ShowLeads.aspx?&list_id=InboundLeads&frm=List'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  leadsAnalyticsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Reports/LeadsDashboard.aspx?subtab=prog_reports'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.displaySidebarHide();
      this.router.navigate(['/dashboard/lead']);
    }
  }

  trackingHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/Anonymous.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  templatesHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Campaigns/TemplateGallery.aspx?setActive=y&maintab=true'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  assetHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Assets/FileManager.aspx?maintab=true&setActive=y'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  variablesHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/AccountVariables.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  widgetsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/Campaigns/HtmlWidget.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  conditionalSegHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/RuleSegmentManager.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  contentHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/MobileApps/ManageApps.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  matricsHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/MobileMetrics.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  geoFencingHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/List/GeoFencing.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  pushNotificationHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/MobileApps/GenericPushMsg.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  supportHandler(): void {
    window.open(
      'http://support.experiture.com/customer/portal/emails/new?ticket[labels_new]=Experiture',
      '_popup'
    );
  }

  rootUserHandler(): void {
    if (this.commonService.getParamInfo()) {
      const infoObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/AgencyPage.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(infoObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }
}
