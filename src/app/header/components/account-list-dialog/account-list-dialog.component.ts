import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonService } from 'src/app/shared/service/common.service';

@Component({
  selector: 'app-account-list-dialog',
  templateUrl: './account-list-dialog.component.html',
  styleUrls: ['./account-list-dialog.component.scss']
})
export class AccountListDialogComponent implements OnInit {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter();
  loadingModal: boolean;

  // for iFrame
  iFrameSrc: string;

  constructor(private commonService: CommonService) {}

  ngOnInit(): void {
    if (this.commonService.getParamInfo()) {
      this.loadingModal = true;
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/AgencySelection.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      this.iFrameSrc = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    }
  }

  onVisibleChange(event: any): void {
    this.visibleChange.emit(event);
  }

  hideDialog(event: any): void {
    this.visibleChange.emit(false);
  }

  // For iFrame
  iFrameLoadEvent(): void {
    this.loadingModal = false;
  }
}
