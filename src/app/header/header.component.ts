import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  ViewChild
} from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { CommonService } from '../shared/service/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showAccount: boolean;
  showNotification: boolean;
  showSiveNav: boolean;
  clientName = sessionStorage.getItem('CLIENT_NAME');
  companyName = sessionStorage.getItem('CLIENT_NAME');
  @ViewChild('notificationRef') notificationRef: ElementRef;
  @ViewChild('accountRef') accountRef: ElementRef;

  displaySidebar: boolean;
  displayAccountList: boolean;
  isRootUser: boolean;

  @HostListener('document:click', ['$event'])
  clickout(event): void {
    if (!this.notificationRef.nativeElement.contains(event.target)) {
      this.showNotification = false;
    }
    if (!this.accountRef.nativeElement.contains(event.target)) {
      this.showAccount = false;
    }
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    const encryptInfo = sessionStorage.getItem('INFO');
    if (encryptInfo) {
      const decryptObj = this.commonService.decrypt(encryptInfo);
      this.isRootUser = decryptObj.rootUserId > 0;
    }
  }

  logoClicked(): void {
    if (this.commonService.getParamInfo()) {
      const encObj = {
        ...this.commonService.getParamInfo(),
        redirect: '/campaigns/AccountDashboard.aspx'
      };
      const encrypt = this.commonService.encrypt(JSON.stringify(encObj));
      const encodedUri = encodeURIComponent(encrypt);
      window.location.href = `https://dev.experiture.com/Authenticate.aspx?a=${encodedUri}`;
    } else {
      this.router.navigate(['/dashboard']);
    }
  }

  toggleSidebar(): void {
    this.showSiveNav = !this.showSiveNav;
    this.displaySidebar = !this.displaySidebar;
  }

  hideSidebar(): void {
    this.showSiveNav = false;
  }

  toggleAccount(): void {
    this.showAccount = !this.showAccount;
  }

  toggleAccountList(): void {
    this.displayAccountList = !this.displayAccountList;
  }

  toggleNotification(): void {
    this.showNotification = !this.showNotification;
  }

  VersionReleaseNotes(): void {}

  logout(): void {
    // this.router.navigate(['login']);
    this.authService.logout();
  }
}
