import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutoLoginComponent } from './auth/auto-login/auto-login.component';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Experiture | Login' }
  },
  {
    path: 'auto',
    component: AutoLoginComponent,
    data: { title: 'Experiture' }
  },
  {
    path: 'targets',
    loadChildren: () =>
      import('./targets/targets.module').then(m => m.TargetsModule)
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./account-dashboard/account-dashboard.module').then(m => m.AccountDashboardModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
