import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ToasterService } from '../shared/service/toaster.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private toasterService: ToasterService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          if (event.body.status === 'failed') {
            this.toasterService.showError(event.body.message);
          }
        }
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status !== 401) {
          this.toasterService.showError();
        }
        return throwError(error);
      })
    );
  }
}
