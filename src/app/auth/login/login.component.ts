import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, Validators.required)
  });
  inputType = 'password';
  loader: boolean;
  @ViewChild('LoginButton') loginButton: ElementRef;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}

  userLogin(): void {
    this.loader = true;
    this.loginButton.nativeElement.disabled = true;
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.authService.clearStroage();
    // sessionStorage.setItem('EMAIL', email);
    // sessionStorage.setItem('PASSWORD', password);
    this.authService.login(email, password).subscribe(
      res => {
        this.authService.storeDetails(res);
        this.router.navigate(['dashboard']);
        this.loader = false;
      },
      error => {
        this.loader = false;
        this.loginButton.nativeElement.disabled = false;
      }
    );
  }

  showPass(): void {
    this.inputType = this.inputType === 'password' ? 'text' : 'password';
  }

  clearForm(): void {
    this.loginForm.reset();
    this.loginForm.updateValueAndValidity();
  }

  forgetPassword(): void {}
}
