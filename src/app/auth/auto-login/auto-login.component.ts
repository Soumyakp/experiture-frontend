import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/shared/service/common.service';
import { ToasterService } from 'src/app/shared/service/toaster.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auto-login',
  templateUrl: './auto-login.component.html',
  styleUrls: ['./auto-login.component.scss']
})
export class AutoLoginComponent implements OnInit {
  loading: boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private toasterService: ToasterService,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    const encryptParam = this.route.snapshot.queryParamMap.get('a');

    // const encryptStr = encryptParam.split(' ').join('+');

    const decyptObj = this.commonService.decrypt(encryptParam);
    sessionStorage.setItem('INFO', encryptParam);
    this.authService.login(decyptObj.userName, decyptObj.password).subscribe(
      res => {
        this.authService.storeDetails(res);
        this.router.navigate([decyptObj.redirect]);
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.toasterService.showInformation('Something went wrong !');
      }
    );
  }
}
