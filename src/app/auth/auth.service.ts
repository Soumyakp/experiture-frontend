import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, tap } from 'rxjs/operators';

const API_URL = `${environment.API_URL}/Auth/`;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private readonly USER_ID = 'USER_ID';
  private readonly CLIENT_ID = 'CLIENT_ID';
  private readonly CLIENT_NAME = 'CLIENT_NAME';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    return this.http.post(
      `${API_URL}authenticate`,
      {
        username,
        password
      },
      this.httpOptions
    );
  }

  logout(): void {
    // TODO: Implemente the logout api
    // API not available yet
    sessionStorage.clear();
    location.reload();
  }

  storeDetails(data: any): void {
    sessionStorage.setItem(this.JWT_TOKEN, data.jwtToken);
    sessionStorage.setItem(this.REFRESH_TOKEN, data.refreshToken);
    sessionStorage.setItem(this.USER_ID, data.userId);
    sessionStorage.setItem(this.CLIENT_ID, data.clientId);
    sessionStorage.setItem(this.CLIENT_NAME, data.organizationName);
  }

  clearStroage(): void {
    sessionStorage.clear();
  }

  getAuthToken(): string {
    return sessionStorage.getItem(this.JWT_TOKEN);
  }

  isLoggedIn(): boolean {
    return !!this.getAuthToken();
  }

  private storeToken(jwtToken: string, refreshToken: string): void {
    sessionStorage.setItem(this.JWT_TOKEN, jwtToken);
    sessionStorage.setItem(this.REFRESH_TOKEN, refreshToken);
  }

  refreshToken(): Observable<any> {
    return this.http
      .post<any>(
        `${API_URL}refresh-token`,
        {
          token: sessionStorage.getItem(this.REFRESH_TOKEN)
        },
        this.httpOptions
      )
      .pipe(
        map(res => {
          this.storeToken(res.jwtToken, res.refreshToken);
          return res;
        })
      );
  }
}
