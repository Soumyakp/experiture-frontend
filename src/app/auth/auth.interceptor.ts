import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { ToasterService } from '../shared/service/toaster.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private toasterService: ToasterService
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (!request.url.endsWith('refresh-token')) {
      request = this.addToken(request);
    }
    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
        }
      }),
      catchError((error: HttpErrorResponse) => {
        return this.handleInvalidToken(error, request, next);
      })
    );
  }

  private addToken(request: HttpRequest<any>): HttpRequest<any> {
    const authToken = this.authService.getAuthToken();
    if (!!authToken) {
      return request.clone({
        setHeaders: {
          Authorization: `Bearer ${authToken}`
        }
      });
    }
    return request;
  }

  private handleInvalidToken(
    error: HttpErrorResponse,
    request?: HttpRequest<any>,
    next?: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // Invalid token error
    if (
      error.status === 401 &&
      error.statusText === 'Unauthorized' &&
      !request.url.endsWith('refresh-token')
    ) {
      return this.authService.refreshToken().pipe(
        switchMap(() => {
          request = this.addToken(request);
          return next.handle(request);
        })
      );
    } else {
      // this.authService.logout();
      // this.toasterService.showInformation(
      //   'Your session has been expire please login once again !',
      //   'Session Expire'
      // );
    }
    return throwError(error);
  }
}
